﻿using System;
using System.IO;
using System.Configuration;
using System.Threading;
using TemplateManager;
using System.ServiceProcess;
using System.Diagnostics;
using System.Collections.Generic;

namespace JobProcessor
{
    public class WorkWatcher
    {
        string workDefinitionPath = string.Empty;
        string workResultPath = string.Empty;
        string configResultPath = string.Empty;
        string AutoCode_Configuration = string.Empty;
        string schemaPath = string.Empty;
        string dictionaryPath = string.Empty;
        string microappPath = string.Empty;
        string servicePath = string.Empty;
        string modelPath = string.Empty;
        string routingPath = string.Empty;
        Dictionary<string, string> paths;

        //Worker worker;
        WorkerWhiteSkin worker;

        EventLog appLog = new EventLog("Application");

        ConfigManager configurationManager;

        string workDirectory;


        public WorkWatcher()
        {
            schemaPath = System.Configuration.ConfigurationManager.AppSettings.Get("schemaPath");
            dictionaryPath = System.Configuration.ConfigurationManager.AppSettings.Get("dictionaryPath");
            microappPath = System.Configuration.ConfigurationManager.AppSettings.Get("microappPath");
            servicePath = System.Configuration.ConfigurationManager.AppSettings.Get("servicePath");
            modelPath = System.Configuration.ConfigurationManager.AppSettings.Get("modelPath");
            routingPath = System.Configuration.ConfigurationManager.AppSettings.Get("routingPath");

            paths = new Dictionary<string, string>();
            paths.Add("schemaPath", schemaPath);
            paths.Add("dictionaryPath", dictionaryPath);
            paths.Add("microappPath", microappPath);
            paths.Add("servicePath", servicePath);
            paths.Add("modelPath", modelPath);
            paths.Add("routingPath", routingPath);

            workDirectory = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), "SpxCodeGen");

            configurationManager = new ConfigManager(paths, workDirectory);

            if (!EventLog.SourceExists("LogAutoCode"))
            {
                EventLog.CreateEventSource("LogAutoCode", "Application");
            }

            appLog.Source = "LogAutoCode";
        }

        public void Start() {

            appLog.WriteEntry("AutoCode iniciado", EventLogEntryType.Information);

            //worker = new Worker();
            worker = new WorkerWhiteSkin();

            
            Directory.CreateDirectory(workDirectory);

            appLog.WriteEntry($"Directorio de trabajo: {workDirectory}", EventLogEntryType.Information);

            if (File.Exists(Path.Combine(workDirectory, "WorkDefinition.json")))
            {
                File.Delete(Path.Combine(workDirectory, "WorkDefinition.json"));
            }

            if (File.Exists(Path.Combine(workDirectory, "WorkConfiguration.json")))
            {
                File.Delete(Path.Combine(workDirectory, "WorkConfiguration.json"));
            }

            if (File.Exists(Path.Combine(workDirectory, "AutoCode_Configuration.json")))
            {
                File.Delete(Path.Combine(workDirectory, "AutoCode_Configuration.json"));
            }            

            workDefinitionPath = Path.Combine(workDirectory, "WorkDefinition.json");
            workResultPath = Path.Combine(workDirectory, "Result.json");
            configResultPath = Path.Combine(workDirectory, "ResultConfiguration.json");
            AutoCode_Configuration = Path.Combine(workDirectory,"AutoCode_Configuration.json");

            paths.Add("workDefinitionPath", workDefinitionPath);
            paths.Add("workResultPath", workDefinitionPath);
            paths.Add("configResultPath", workDefinitionPath);

            var watcher = new FileSystemWatcher(workDirectory);
            watcher.NotifyFilter = NotifyFilters.Attributes
                                | NotifyFilters.CreationTime
                                | NotifyFilters.DirectoryName
                                | NotifyFilters.FileName
                                | NotifyFilters.LastAccess
                                | NotifyFilters.LastWrite
                                | NotifyFilters.Security
                                | NotifyFilters.Size;

            watcher.Created += OnCreated;

            watcher.Filter = "Work*.json";
            watcher.IncludeSubdirectories = false;
            watcher.EnableRaisingEvents = true;
        }

        public void Stop() { }

        private void OnCreated(object sender, FileSystemEventArgs e)
        {
            try
            {
                if (File.Exists(workResultPath)) {
                    File.Delete(workResultPath);
                }

                if (File.Exists(configResultPath))
                {
                    File.Delete(configResultPath);
                }

                if (File.Exists(AutoCode_Configuration))    
                {
                    File.Delete(AutoCode_Configuration);
                }

                Console.WriteLine("Creado");
                var fullPath = e.FullPath;

                if (Path.GetFileName(fullPath).Equals("WorkDefinition.json", StringComparison.InvariantCultureIgnoreCase)) {
                    worker.ExecuteWork(workDefinitionPath, configurationManager._WorkRoutes);
                    File.WriteAllLines(workResultPath, new string[] { "Módulo creado" });
                }

                if (Path.GetFileName(fullPath).Equals("WorkConfiguration.json", StringComparison.InvariantCultureIgnoreCase))
                {
                    configurationManager.ProcessConfigRequest(fullPath);
                }

                File.Delete(fullPath);
                Console.WriteLine("Procesado: {0}", fullPath);
            }
            catch (Exception ex)
            {
                if(File.Exists(workDefinitionPath))
                {
                    File.Delete(workDefinitionPath);
                }

                if (File.Exists(workResultPath))
                {
                    File.Delete(workResultPath);
                }

                appLog.WriteEntry($"Error {ex.Message}", EventLogEntryType.Information);

                File.WriteAllLines(workResultPath, new string[] { "Error", ex.Message });
            }
        }

    }
}
