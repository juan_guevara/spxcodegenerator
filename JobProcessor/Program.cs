﻿using System;
using System.Linq;
using System.Configuration;
using Microsoft.Win32;
using System.Globalization;
using Topshelf;
using System.IO;

namespace JobProcessor
{
    class Program
    {
        static void Main(string[] args)
        {
            string Description = ConfigurationManager.AppSettings["ServiceDescription"].ToString();
            string DisplayName = ConfigurationManager.AppSettings["ServiceDisplayName"].ToString();
            string ServiceName = ConfigurationManager.AppSettings["ServiceName"].ToString();

            HostFactory.Run(x =>
            {
                x.Service<WorkWatcher>(s =>
                {
                    s.ConstructUsing(name => new WorkWatcher());
                    s.WhenStarted(ww => ww.Start());
                    s.WhenStopped(ww => ww.Stop());
                });
                x.StartAutomaticallyDelayed();
                x.RunAsLocalSystem();
                x.SetDescription(string.Format("{0} 1.0.0", Description));
                x.SetDisplayName(string.Format("{0} 1.0.0", DisplayName));
                x.SetServiceName(ServiceName);
            });

            Console.ReadLine();
        }

        #region [ inicio automatico de windows ]

        public static void CheckParameters(string[] args)
        {
            if (args.Any())
            {
                foreach (var strCad in args)
                {
                    switch (strCad)
                    {
                        case "-S":
                        case "-s":
                            AddApplicationToAllUserStartup();
                            break;
                        case "-D":
                        case "-d":
                            RemoveApplicationFromAllUserStartup();
                            break;
                    }
                }
            }
        }

        public static void AddApplicationToAllUserStartup()
        {
            var serviceName = ConfigurationManager.AppSettings["ServiceName"].ToString(CultureInfo.InvariantCulture);
            using (var key = Registry.LocalMachine.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true))
            {
                if (key != null)
                    key.SetValue(serviceName, Directory.GetCurrentDirectory());
            }
        }

        public static void RemoveApplicationFromAllUserStartup()
        {
            var serviceName = ConfigurationManager.AppSettings["ServiceName"].ToString(CultureInfo.InvariantCulture);
            using (var key = Registry.LocalMachine.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true))
            {
                if (key != null)
                    key.DeleteValue(serviceName, false);
            }
        }

        #endregion
    }
}
