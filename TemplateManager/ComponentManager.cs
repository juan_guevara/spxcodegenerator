﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace TemplateManager
{
    public enum Language
    {
        Spanish,
        English
    }

    public class ComponentManager
    {
        public void CreatePrincipalComponent(ComponentDefinition job, ModuleDefinition module, List<string> componentNames, Dictionary<string, string> workerRoutes)
        {
            Console.WriteLine(workerRoutes);

            string ComponentName = job.ComponentName;

            string templatesPath = System.Configuration.ConfigurationManager.AppSettings.Get("templatesPath");
            string rendererTemplatePath = Path.Combine(templatesPath, "typescript.json");
            string routingappTemplatePath = Path.Combine(templatesPath, "typescript.json");
            string componentTemplatePath = Path.Combine(templatesPath, "typescript.json");
            string componentSpecTemplatePath = Path.Combine(templatesPath, "typescript.json");
            string modelTemplatePath = Path.Combine(templatesPath, "typescript.json");
            string htmlTemplatePath = Path.Combine(templatesPath, "html.json");
            string serviceTemplatePath = Path.Combine(templatesPath, "typescript.json");
            string routingTemplatePath = Path.Combine(templatesPath, "typescript.json");
            string moduleTemplatePath = Path.Combine(templatesPath, "typescript.json");
            string moduleSpecTemplatePath = Path.Combine(templatesPath, "typescript.json");

            string dictionaryPath = workerRoutes["dictionaryPath"];
            string schemaPath = workerRoutes["schemaPath"];
            string appPath = Path.Combine(workerRoutes["microappPath"], "microapps", module.App);
            string translatorPath = Path.Combine(workerRoutes["microappPath"], "microapps", module.App, "core");
            string servicePath = Path.Combine(workerRoutes["microappPath"], "microapps", module.App, "shared", "services");
            string modelPath = Path.Combine(workerRoutes["microappPath"], "microapps", module.App, "shared", "models");

            //string componentPath = Path.Combine(appPath, "pages", module.ModuleName, ComponentName.ToLower(), string.Format("lst{0}", ComponentName.ToLower()));
            string componentPath = Path.Combine(appPath, "pages", ComponentName.ToLower(), string.Format("lst{0}", ComponentName.ToLower()));
            //string searchResultPath = Path.Combine(appPath, "pages", module.ModuleName, ComponentName.ToLower(), $"SR{ComponentName.ToLower()}");
            string searchResultPath = Path.Combine(appPath, "pages", ComponentName.ToLower(), $"SR{ComponentName.ToLower()}");
            //string detailPath = Path.Combine(appPath, "pages", module.ModuleName, ComponentName.ToLower(), $"DT{ComponentName.ToLower()}");
            string detailPath = Path.Combine(appPath, "pages", ComponentName.ToLower(), $"DT{ComponentName.ToLower()}");

            string routingPath = workerRoutes["routingPath"];

            FilesBuilder fb = new FilesBuilder();

            List<string> paths = new List<string>(){
                    appPath,
                    modelPath,
                    servicePath,
                    translatorPath
                };

            foreach (var path in paths)
            {
                CreateDirectory(path);
            }

            Dictionary<string, string> patterns = new Dictionary<string, string>();
            CreateDictionary(Path.Combine($"{dictionaryPath}{ToUpperFirstLetter(module.App)}", "es.json"), job.ComponentName, job.ObjectsList, Language.Spanish, job);
            CreateDictionary(Path.Combine($"{dictionaryPath}{ToUpperFirstLetter(module.App)}", "en.json"), job.ComponentName, job.ObjectsList, Language.English, job);

            //Traducciones
            patterns = new Dictionary<string, string>(){
                    {@"\${1:([^\}]*)\}", module.App}
                };
            File.WriteAllText(Path.Combine(translatorPath, "missing-translation-handler.ts"), fb.BuildContent(serviceTemplatePath, "translationhandler", patterns));


            //componente searchresult
            CreateDirectory(searchResultPath);
            File.WriteAllText(Path.Combine(searchResultPath, $"SR{ComponentName.ToLower()}.component.scss"), fb.BuildContent(componentTemplatePath, "searchresult_scss", new Dictionary<string, string>()));
            File.WriteAllText(Path.Combine(searchResultPath, $"SR{ComponentName.ToLower()}.component.html"), fb.BuildContent(htmlTemplatePath, "searchresult_html", new Dictionary<string, string>()));

            patterns = new Dictionary<string, string>(){
                    {@"\${1:([^\}]*)\}", $"SR{ComponentName}Component"},
                    {@"\${2:([^\}]*)\}", $"{ComponentName}"}
            };
            File.WriteAllText(Path.Combine(searchResultPath, $"SR{ComponentName.ToLower()}.component.spec.ts"), fb.BuildContent(componentTemplatePath, "searchresult_spec", patterns));

            string columns = CreateAgGridColumnLines(job.ObjectsList, job.ComponentName, rendererTemplatePath);

            patterns = new Dictionary<string, string>(){
                    {@"\${1:([^\}]*)\}", ComponentName.ToLower()},
                    {@"\${2:([^\}]*)\}", columns},
                    {@"\${3:([^\}]*)\}", job.SelectionCode }
            };
            File.WriteAllText(Path.Combine(searchResultPath, $"SR{ComponentName.ToLower()}.component.ts"), fb.BuildContent(componentTemplatePath, "searchresult_component", patterns));

            //componente detalle
            CreateDirectory(detailPath);

            patterns = new Dictionary<string, string>(){
                    {@"\${1:([^\}]*)\}", job.GridConfiguration != null && job.GridConfiguration.SpanLabel != 0 ? $"{job.GridConfiguration.SpanLabel}" : "2"},
                    {@"\${2:([^\}]*)\}", job.GridConfiguration != null && job.GridConfiguration.SpanLabel != 0 ? $"{job.GridConfiguration.SpanField}" : "4"},
                    {@"\${3:([^\}]*)\}", job.GridConfiguration != null && !string.IsNullOrEmpty(job.GridConfiguration.AutoCompleteBehavior) ? $"{job.GridConfiguration.AutoCompleteBehavior}" : "auto-fit"}
            };
            File.WriteAllText(Path.Combine(detailPath, $"DT{ComponentName.ToLower()}.component.scss"), fb.BuildContent(componentTemplatePath, "detail_scss", patterns));

            string groups = CreateDetailGroups(job.ObjectsList, ComponentName, htmlTemplatePath, module);
            patterns = new Dictionary<string, string>(){
                    {@"\${1:([^\}]*)\}", groups},
                    {@"\${2:([^\}]*)\}", job.SummaryName},
                    {@"\${3:([^\}]*)\}", job.SummaryCode}
            };
            File.WriteAllText(Path.Combine(detailPath, $"DT{ComponentName.ToLower()}.component.html"), fb.BuildContent(htmlTemplatePath, "detail_html", patterns));

            patterns = new Dictionary<string, string>(){
                    {@"\${1:([^\}]*)\}", ComponentName.ToLower()},
                    {@"\${2:([^\}]*)\}", job.SummaryCode},
                    {@"\${3:([^\}]*)\}", CreateClickCollapsibles(componentTemplatePath, module)},
                    {@"\${4:([^\}]*)\}", CreateDataVariables(componentTemplatePath, module)},
                    {@"\${5:([^\}]*)\}", CreateViewChilds(job.ObjectsList)},
                    {@"\${6:([^\}]*)\}", CreateViewChildsBinding(job.ObjectsList)},
                    {@"\${7:([^\}]*)\}", CreateChildMethodsComponent(componentTemplatePath, module)},
                    {@"\${8:([^\}]*)\}", CreateChildLoadsInDataChange(module)},
                    {@"\${9:([^\}]*)\}", CreateDataVariablesInput(componentTemplatePath, module)}
            };
            File.WriteAllText(Path.Combine(detailPath, $"DT{ComponentName.ToLower()}.component.ts"), fb.BuildContent(componentTemplatePath, "detail_component", patterns));

            CreateDirectory(componentPath);

            //scss
            File.WriteAllText(Path.Combine(componentPath, string.Format(@"lst{0}.component.scss", ComponentName.ToLower())), fb.BuildContent(componentTemplatePath, "scss", patterns));

            //html
            if (job.ObjectsList.Count(x => x.ObjectType.Equals("ListColumn", StringComparison.InvariantCultureIgnoreCase)) > 0)
            {
                patterns = HtmlPatternsGenerator.BuildPatterns(job, htmlTemplatePath, module);
                File.WriteAllText(Path.Combine(componentPath, string.Format(@"lst{0}.component.html", ComponentName.ToLower())), fb.BuildContent(htmlTemplatePath, "Html_list", patterns));
            }

            //Componente
            patterns = new Dictionary<string, string>(){
                     {@"\${1:([^\}]*)\}", ComponentName.ToLower()}
                    ,{@"\${2:([^\}]*)\}", string.Format("Lst{0}",ComponentName)}
                    ,{@"\${3:([^\}]*)\}", job.SummaryCode}
                    ,{@"\${4:([^\}]*)\}", job.SummaryName}
                    ,{@"\${5:([^\}]*)\}", module.App}
                };
            File.WriteAllText(Path.Combine(componentPath, string.Format(@"lst{0}.component.ts", ComponentName.ToLower())), fb.BuildContent(componentTemplatePath, "componente", patterns));


            //Component_spec
            patterns = new Dictionary<string, string>(){
                    {@"\${1:([^\}]*)\}", ComponentName},
                    {@"\${2:([^\}]*)\}", ComponentName.ToLower()}
                };
            File.WriteAllText(Path.Combine(componentPath, string.Format(@"lst{0}.component.spec.ts", ComponentName.ToLower())), fb.BuildContent(componentSpecTemplatePath, "component_spec", patterns));

            //Modelo
            patterns = new Dictionary<string, string>(){
                    {@"\${1:([^\}]*)\}", ComponentName},
                    {@"\${2:([^\}]*)\}", CreateModelLines(job.ObjectsList, "FormField")},
                };
            File.WriteAllText(Path.Combine(modelPath, string.Format(@"{0}.model.ts", ComponentName.ToLower())), fb.BuildContent(modelTemplatePath, "modelo", patterns));

            //Servicio
            patterns = new Dictionary<string, string>(){
                    {@"\${1:([^\}]*)\}", ComponentName}
                   ,{@"\${2:([^\}]*)\}", job.SearchFunction}
                   ,{@"\${3:([^\}]*)\}", job.Post}
                   ,{@"\${4:([^\}]*)\}", job.Put}
                   ,{@"\${5:([^\}]*)\}", job.Delete}
                   ,{@"\${6:([^\}]*)\}", job.SummaryFunction}
                   ,{@"\${7:([^\}]*)\}", CreateServiceChildMethods(module, serviceTemplatePath)}
                };
            File.WriteAllText(Path.Combine(servicePath, string.Format("{0}.service.ts", ComponentName.ToLower())), fb.BuildContent(serviceTemplatePath, "servicio", patterns));
        }

        private string CreateDataVariablesInput(string componentTemplatePath, ModuleDefinition module)
        {
            string result = string.Empty;

            foreach (var component in module.ComponentList)
            {
                if (!component.Principal)
                {
                    result = $"{result}\r\n\timport {{ {component.ComponentName.ToLower()}Model }} from '../../../shared/models/{component.ComponentName.ToLower()}.model';";
                }
            }

            return result;
        }

        private string CreateChildLoadsInDataChange(ModuleDefinition module)
        {
            string result = string.Empty;

            foreach (var component in module.ComponentList)
            {
                if (!component.Principal)
                {
                    result = $"{result}\r\n\tthis.Load{component.ComponentName.ToLower()}(null);";
                }
            }

            return result;
        }

        private string CreateViewChildsBinding(ObjectDefinition[] objectsList)
        {
            string result;
            result = string.Empty;

            string templateType = string.Empty;

            foreach (var item in objectsList)
            {

                if (!string.IsNullOrEmpty(item.CellTemplate))
                {
                    if (item.CellTemplate.Equals("GenericAutoComplete"))
                    {
                        result = $"{result}\r\n\tif(this.AC{item.Name}){{\r\n\t\tthis.data.{item.Name} = this.AC{item.Name}.Code;\r\n\t}}";
                    }

                    if (item.CellTemplate.Equals("GenericSelectBox"))
                    {
                        result = $"{result}\r\n\tif(this.SB{item.Name} && this.SB{item.Name}.touched ){{\r\n\t\tthis.data.{item.Name} = this.SB{item.Name}.Code;\r\n\t}}";
                    }
                }
            }

            return result;
        }

        private string CreateViewChilds(ObjectDefinition[] objectsList)
        {
            string result;
            result = string.Empty;

            string templateType = string.Empty;

            foreach (var item in objectsList)
            {

                if (!string.IsNullOrEmpty(item.CellTemplate))
                {
                    if (item.CellTemplate.Equals("GenericAutoComplete"))
                    {
                        result = $"{result}\r\n @ViewChild('AutoComplete{item.Name}', {{ static: false}}) AC{item.Name}: GenericAutocompleteComponent;";
                    }

                    if (item.CellTemplate.Equals("GenericSelectBox"))
                    {
                        result = $"{result}\r\n @ViewChild('AutoSelectBox{item.Name}', {{ static: false}}) SB{item.Name}: GenericSelectBoxComponent;";
                    }
                }
            }

            return result;
        }

        private string CreateDataVariables(string componentTemplatePath, ModuleDefinition module)
        {
            string result = string.Empty;
            FilesBuilder fb = new FilesBuilder();

            foreach (var component in module.ComponentList)
            {
                if (!component.Principal)
                {
                    Dictionary<string, string> patterns = new Dictionary<string, string>(){
                    {@"\${1:([^\}]*)\}", component.ComponentName.ToLower()}
                };

                    result = $"{result}\n{fb.BuildContent(componentTemplatePath, "childComponentDataVariable", patterns)}";
                }

                if (!component.Principal)
                {
                    Dictionary<string, string> patterns = new Dictionary<string, string>(){
                    {@"\${1:([^\}]*)\}", component.ComponentName.ToLower()}
                };

                    result = $"{result}\n{fb.BuildContent(componentTemplatePath, "childComponentEditionDataVariable", patterns)}";
                }
            }

            return result;
        }

        private string CreateServiceChildMethods(ModuleDefinition module, string serviceTemplatePath)
        {
            string result = string.Empty;
            FilesBuilder fb = new FilesBuilder();
            foreach (ComponentDefinition component in module.ComponentList)
            {
                if (!component.Principal && !string.IsNullOrEmpty(component.SearchFunction) && !string.IsNullOrWhiteSpace(component.SearchFunction))
                {
                    Dictionary<string, string> patterns = new Dictionary<string, string>(){
                    {@"\${1:([^\}]*)\}", component.ComponentName}
                   ,{@"\${2:([^\}]*)\}", component.SearchFunction}
                };
                    result = $"{result}\r\n{fb.BuildContent(serviceTemplatePath, "getChildComponentData", patterns)}";
                }

                if (!component.Principal && !string.IsNullOrEmpty(component.GetBy) && !string.IsNullOrWhiteSpace(component.GetBy))
                {
                    if (component.GetBy.Contains("contains")) {
                        throw new Exception("Restriccion de uso generador de código \"No se permite el método contains en la petición GetBy\"");
                    }

                    Dictionary<string, string> patterns = new Dictionary<string, string>(){
                    {@"\${1:([^\}]*)\}", component.ComponentName}
                   ,{@"\${2:([^\}]*)\}", component.GetBy}
                };
                    result = $"{result}\r\n{fb.BuildContent(serviceTemplatePath, "getByChildComponentData", patterns)}";
                }

                if (!component.Principal && !string.IsNullOrEmpty(component.Post) && !string.IsNullOrWhiteSpace(component.Post))
                {
                    Dictionary<string, string> patterns = new Dictionary<string, string>(){
                    {@"\${1:([^\}]*)\}", component.ComponentName}
                   ,{@"\${2:([^\}]*)\}", component.Post}
                };
                    result = $"{result}\r\n{fb.BuildContent(serviceTemplatePath, "postChildComponentData", patterns)}";
                }

                if (!component.Principal && !string.IsNullOrEmpty(component.Delete) && !string.IsNullOrWhiteSpace(component.Delete))
                {
                    Dictionary<string, string> patterns = new Dictionary<string, string>(){
                    {@"\${1:([^\}]*)\}", component.ComponentName}
                   ,{@"\${2:([^\}]*)\}", component.Delete}
                };
                    result = $"{result}\r\n{fb.BuildContent(serviceTemplatePath, "deleteChildComponentData", patterns)}";
                }

                if (!component.Principal && !string.IsNullOrEmpty(component.Put) && !string.IsNullOrWhiteSpace(component.Put))
                {
                    Dictionary<string, string> patterns = new Dictionary<string, string>(){
                    {@"\${1:([^\}]*)\}", component.ComponentName}
                   ,{@"\${2:([^\}]*)\}", component.Put}
                };
                    result = $"{result}\r\n{fb.BuildContent(serviceTemplatePath, "putChildComponentData", patterns)}";
                }
            }

            return result;
        }

        public void CreateComponent(ComponentDefinition job, ModuleDefinition module, Dictionary<string, string> workerRoutes, string principalComponentName)
        {
            string appPath = Path.Combine(workerRoutes["microappPath"], "microapps", module.App);
            string componentName = job.ComponentName;
            //string componentPath = Path.Combine(appPath, "pages", module.ModuleName, principalComponentName, componentName.ToLower());
            string componentPath = Path.Combine(appPath, "pages", principalComponentName, componentName.ToLower());

            string modelPath = Path.Combine(workerRoutes["microappPath"], "microapps", module.App, "shared", "models");

            string dictionaryPath = workerRoutes["dictionaryPath"];
            string templatesPath = System.Configuration.ConfigurationManager.AppSettings.Get("templatesPath");
            string modelTemplatePath = Path.Combine(templatesPath, "typescript.json");
            //string moduleTemplatePath = Path.Combine(templatesPath, "typescript.json");

            Dictionary<string, string> patterns = new Dictionary<string, string>();
            CreateDictionary(Path.Combine($"{dictionaryPath}{ToUpperFirstLetter(module.App)}", "es.json"), job.ComponentName, job.ObjectsList, Language.Spanish, job);
            CreateDictionary(Path.Combine($"{dictionaryPath}{ToUpperFirstLetter(module.App)}", "en.json"), job.ComponentName, job.ObjectsList, Language.English, job);

            CreateDirectory(componentPath);


            string componentTemplatePath = Path.Combine(templatesPath, "typescript.json");
            string cssTemplatePath = Path.Combine(templatesPath, "css.json");

            string htmlTemplatePath = Path.Combine(templatesPath, "html.json");

            FilesBuilder fb = new FilesBuilder();

            string childComponentTitle = string.Empty;
            string widthPopup = "500";
            string heigthPopup = "500";
            string traduction = " ";
            List<ObjectDefinition> objects = job.ObjectsList.ToList<ObjectDefinition>();
            if (objects.Exists(x => x.ObjectType.Equals("Popup", StringComparison.InvariantCultureIgnoreCase)))
            {
                ObjectDefinition popupDefinition = objects.Find(x => x.ObjectType.Equals("Popup", StringComparison.InvariantCultureIgnoreCase));
                childComponentTitle = popupDefinition.Name;
                traduction = $"{{{{ 'lst{componentName.ToLower()}.{childComponentTitle.ToUpper()}' | translate }}}}";
                widthPopup = popupDefinition.Width > 0 ? $"{popupDefinition.Width}" : widthPopup;
                heigthPopup = popupDefinition.Heigth > 0 ? $"{popupDefinition.Heigth}" : heigthPopup;
            }

            //Modelo
            patterns = new Dictionary<string, string>(){
                    {@"\${1:([^\}]*)\}", componentName},
                    {@"\${2:([^\}]*)\}", CreateModelLines(job.ObjectsList, "FormField")},
                };
            string model = fb.BuildContent(modelTemplatePath, "modelo", patterns);
            model = model.Replace(":Number;", ":number;");
            model = model.Replace(":Date;", ":string|number|Date;");
            model = model.Replace(":Number;", ":number;");
            File.WriteAllText(Path.Combine(modelPath, string.Format(@"{0}.model.ts", componentName.ToLower())), model);

            //html
            string htmlPath = Path.Combine(componentPath, $"{componentName.ToLower()}.component.html");
            string controls = CreateDetailControls(job.ObjectsList, componentName, htmlTemplatePath);
            controls = controls.Replace("\"Data.", "\"editionData.");
            patterns = new Dictionary<string, string>() {
                {@"\${1:([^\}]*)\}", traduction},
                {@"\${2:([^\}]*)\}",  widthPopup},
                {@"\${3:([^\}]*)\}",  heigthPopup},
                {@"\${4:([^\}]*)\}",  controls}
            };
            File.WriteAllText(htmlPath, fb.BuildContent(htmlTemplatePath, "child_componenthtml", patterns));

            //ts
            string rendererTemplatePath = Path.Combine(templatesPath, "typescript.json");
            string tsPath = Path.Combine(componentPath, $"{componentName.ToLower()}.component.ts");

            string deleteColumnName = string.Empty;
            if (job.ObjectsList.ToList<ObjectDefinition>().Exists(x => x.ObjectType == "DeleteColumn"))
            {
                deleteColumnName = job.ObjectsList.First<ObjectDefinition>(x => x.ObjectType == "DeleteColumn").Name;
            }

            string editColumnName = string.Empty;
            if (job.ObjectsList.ToList<ObjectDefinition>().Exists(x => x.ObjectType == "EditColumn"))
            {
                editColumnName = job.ObjectsList.First<ObjectDefinition>(x => x.ObjectType == "EditColumn").Name;
            }

            patterns = new Dictionary<string, string>() {
                {@"\${1:([^\}]*)\}", componentName},
                {@"\${2:([^\}]*)\}", CreateAgGridColumnLines(job.ObjectsList,job.ComponentName, rendererTemplatePath)}
            };

            if (!string.IsNullOrEmpty(deleteColumnName))
            {
                patterns.Add(@"\${3:([^\}]*)\}", deleteColumnName);
            }

            if (!string.IsNullOrEmpty(editColumnName))
            {
                patterns.Add(@"\${4:([^\}]*)\}", editColumnName);
            }

            File.WriteAllText(tsPath, fb.BuildContent(componentTemplatePath, "child_componentts", patterns));

            //scss
            string scssPath = Path.Combine(componentPath, $"{componentName.ToLower()}.component.scss");
            File.WriteAllText(scssPath, fb.BuildContent(cssTemplatePath, "child_scss", new Dictionary<string, string>()));

            ////componente searchresult
            //CreateDirectory(searchResultPath);
            //File.WriteAllText(Path.Combine(searchResultPath, $"SR{ComponentName.ToLower()}.component.scss"), fb.BuildContent(componentTemplatePath, "searchresult_scss", new Dictionary<string, string>()));
            //File.WriteAllText(Path.Combine(searchResultPath, $"SR{ComponentName.ToLower()}.component.html"), fb.BuildContent(htmlTemplatePath, "searchresult_html", new Dictionary<string, string>()));

            //patterns = new Dictionary<string, string>(){
            //        {@"\${1:([^\}]*)\}", $"SR{ComponentName}Component"},
            //        {@"\${2:([^\}]*)\}", $"{ComponentName}"}
            //};
            //File.WriteAllText(Path.Combine(searchResultPath, $"SR{ComponentName.ToLower()}.component.spec.ts"), fb.BuildContent(componentTemplatePath, "searchresult_spec", patterns));

            //string columns = CreateAgGridColumnLines(job.ObjectsList, job.ComponentName, rendererTemplatePath);

            //patterns = new Dictionary<string, string>(){
            //        {@"\${1:([^\}]*)\}", ComponentName.ToLower()},
            //        {@"\${2:([^\}]*)\}", columns},
            //        {@"\${3:([^\}]*)\}", job.SearchCode }
            //};
            //File.WriteAllText(Path.Combine(searchResultPath, $"SR{ComponentName.ToLower()}.component.ts"), fb.BuildContent(componentTemplatePath, "searchresult_component", patterns));
        }

        //private string CreateDetailChildControls(ObjectDefinition[] objectDefinitions, string componentName, string htmlTemplatePath)
        //{

        //    string result = string.Empty;

        //    Dictionary<string, string> patterns = new Dictionary<string, string>();
        //    FilesBuilder fb = new FilesBuilder();
        //    string controls = string.Empty;

        //    foreach (var control in objectDefinitions.Where<ObjectDefinition>(x => x.ObjectType.Equals("FormField", StringComparison.InvariantCultureIgnoreCase)))
        //    {

        //        patterns = new Dictionary<string, string>();

        //        if (string.IsNullOrEmpty(control.CellTemplate))
        //        {
        //            patterns.Add(@"\${1:([^\}]*)\}", control.Name);
        //            if (!string.IsNullOrEmpty(control.BehaviorInEdition) && control.BehaviorInEdition.Equals("disabled", StringComparison.InvariantCultureIgnoreCase))
        //            {
        //                patterns.Add(@"\${2:([^\}]*)\}", "[disabled]=\"!newRow\"");
        //            }
        //            string field = fb.BuildContent(htmlTemplatePath, $"detail_{control.Type}_field", patterns);

        //            patterns = new Dictionary<string, string>();
        //            patterns.Add(@"\${1:([^\}]*)\}", componentName);
        //            patterns.Add(@"\${2:([^\}]*)\}", control.Name.ToUpper());
        //            patterns.Add(@"\${3:([^\}]*)\}", field);
        //            controls = $"{controls}\n\r{fb.BuildContent(htmlTemplatePath, "detail_control", patterns)}";
        //        }
        //        else
        //        {

        //            if (control.CellTemplate.Equals("GenericAutoComplete", StringComparison.InvariantCultureIgnoreCase))
        //            {
        //                patterns.Add(@"\${1:([^\}]*)\}", $"{{{{\"lst{componentName.ToLower()}.{control.Name.ToUpper()}\" | translate }}}}");
        //                patterns.Add(@"\${2:([^\}]*)\}", control.Name);
        //                patterns.Add(@"\${3:([^\}]*)\}", control.TemplateParameters.FirstOrDefault(x => x.Name.Equals("columnaCodigo", StringComparison.InvariantCultureIgnoreCase)).Value);
        //                patterns.Add(@"\${4:([^\}]*)\}", control.TemplateParameters.FirstOrDefault(x => x.Name.Equals("columnaNombre", StringComparison.InvariantCultureIgnoreCase)).Value);
        //                patterns.Add(@"\${5:([^\}]*)\}", control.TemplateParameters.FirstOrDefault(x => x.Name.Equals("controlador", StringComparison.InvariantCultureIgnoreCase)).Value);
        //                patterns.Add(@"\${6:([^\}]*)\}", control.TemplateParameters.FirstOrDefault(x => x.Name.Equals("columnaFiltro", StringComparison.InvariantCultureIgnoreCase)).Value);
        //                patterns.Add(@"\${7:([^\}]*)\}", control.TemplateParameters.FirstOrDefault(x => x.Name.Equals("filtros", StringComparison.InvariantCultureIgnoreCase)).Value);

        //                controls = $"{controls}\n\r{fb.BuildContent(htmlTemplatePath, "detail_generic_autocomplete", patterns)}";
        //            }

        //            if (control.CellTemplate.Equals("GenericSelectBox", StringComparison.InvariantCultureIgnoreCase))
        //            {
        //                patterns.Add(@"\${1:([^\}]*)\}", $"{{{{\"lst{componentName.ToLower()}.{control.Name.ToUpper()}\" | translate }}}}");
        //                patterns.Add(@"\${2:([^\}]*)\}", control.Name);
        //                patterns.Add(@"\${3:([^\}]*)\}", control.TemplateParameters.FirstOrDefault(x => x.Name.Equals("columnaCodigo", StringComparison.InvariantCultureIgnoreCase)).Value);
        //                patterns.Add(@"\${4:([^\}]*)\}", control.TemplateParameters.FirstOrDefault(x => x.Name.Equals("columnaNombre", StringComparison.InvariantCultureIgnoreCase)).Value);
        //                patterns.Add(@"\${5:([^\}]*)\}", control.TemplateParameters.FirstOrDefault(x => x.Name.Equals("controlador", StringComparison.InvariantCultureIgnoreCase)).Value);
        //                patterns.Add(@"\${6:([^\}]*)\}", control.TemplateParameters.FirstOrDefault(x => x.Name.Equals("columnaFiltro", StringComparison.InvariantCultureIgnoreCase)).Value);
        //                patterns.Add(@"\${7:([^\}]*)\}", control.TemplateParameters.FirstOrDefault(x => x.Name.Equals("filtros", StringComparison.InvariantCultureIgnoreCase)).Value);

        //                controls = $"{controls}\n\r{fb.BuildContent(htmlTemplatePath, "detail_generic_selectbox", patterns)}";
        //            }

        //        }


        //    }

        //    return controls;
        //}

        private string CreateChildMethodsComponent(string componentTemplatePath, ModuleDefinition module)
        {
            string result = string.Empty;
            FilesBuilder fb = new FilesBuilder();

            string principalName = module.ComponentList.First<ComponentDefinition>(x => x.Principal).ComponentName.ToLower();

            foreach (var component in module.ComponentList)
            {
                if (!component.Principal)
                {
                    Dictionary<string, string> patterns = new Dictionary<string, string>(){
                        {@"\${1:([^\}]*)\}", component.ComponentName.ToLower()},
                        {@"\${2:([^\}]*)\}", CreateAddScript(componentTemplatePath, principalName.ToLower(), component)},
                        {@"\${3:([^\}]*)\}", CreateEditScript(componentTemplatePath, principalName.ToLower(), component)}
                    };

                    result = $"{result}{fb.BuildContent(componentTemplatePath, "savedchildcomponent", patterns)}";
                }

                if (!component.Principal && !string.IsNullOrEmpty(component.Delete))
                {
                    Dictionary<string, string> patterns = new Dictionary<string, string>(){
                        {@"\${1:([^\}]*)\}", component.ComponentName.ToLower()},
                        {@"\${2:([^\}]*)\}",  principalName.ToLower()}
                    };

                    result = $"{result}{fb.BuildContent(componentTemplatePath, "deletechildcomponent", patterns)}";
                }

                if (!component.Principal && !string.IsNullOrEmpty(component.Put))
                {
                    if (component.ObjectsList.ToList<ObjectDefinition>().Exists(x => x.ObjectType.Equals("EditColumn", StringComparison.InvariantCultureIgnoreCase)))
                    {
                        string fieldname = component.ObjectsList.First<ObjectDefinition>(x => x.ObjectType.Equals("EditColumn")).Name;
                        
                        Dictionary<string, string> patterns = new Dictionary<string, string>(){
                            {@"\${1:([^\}]*)\}", component.ComponentName.ToLower()},
                            {@"\${2:([^\}]*)\}", principalName.ToLower()},
                            {@"\${3:([^\}]*)\}", fieldname}
                        };

                        result = $"{result}{fb.BuildContent(componentTemplatePath, "loadChildDetail", patterns)}";
                    }
                }

            }

            return result;
        }

        private string CreateEditScript(string componentTemplatePath, string principalName, ComponentDefinition component)
        {
            FilesBuilder fb = new FilesBuilder();
            string result = string.Empty;

            if (!component.Principal && !string.IsNullOrEmpty(component.Put))
            {
                Dictionary<string, string> patterns = new Dictionary<string, string>(){
                        {@"\${1:([^\}]*)\}", component.ComponentName.ToLower()},
                        {@"\${2:([^\}]*)\}", principalName.ToLower()}
                    };

                result = $"{result}{fb.BuildContent(componentTemplatePath, "putchildcomponent", patterns)}";
            }

            return result;
        }

        private string CreateAddScript(string componentTemplatePath, string principalName, ComponentDefinition component)
        {
            FilesBuilder fb = new FilesBuilder();
            string result = string.Empty;

            if (!component.Principal && !string.IsNullOrEmpty(component.Post))
            {
                Dictionary<string, string> patterns = new Dictionary<string, string>(){
                        {@"\${1:([^\}]*)\}", component.ComponentName.ToLower()},
                        {@"\${2:([^\}]*)\}", principalName.ToLower()}
                    };

                result = $"{result}{fb.BuildContent(componentTemplatePath, "postchildcomponent", patterns)}";
            }

            return result;
        }

        private string CreateChildEvents(ComponentDefinition[] componentList)
        {
            string result = string.Empty;

            foreach (var component in componentList) {
                
                if (!component.Principal) {

                    if (!string.IsNullOrEmpty(component.Post)) {
                        result = $"{result} (OnSaved)=\"On{component.ComponentName.ToLower()}Saved($event)\"";
                    }

                    if (!string.IsNullOrEmpty(component.Delete))
                    {
                        result = $"{result} (OnDelete)=\"On{component.ComponentName.ToLower()}Delete($event)\"";
                    }

                    bool existEdithColumn = component.ObjectsList.ToList<ObjectDefinition>().Exists(x => x.ObjectType.Equals("EditColumn"));

                    if (!string.IsNullOrEmpty(component.Put) && existEdithColumn)
                    {
                        result = $"{result} (OnEdit)=\"On{component.ComponentName.ToLower()}Put($event)\"";
                    }
                }

            }

            return result;
        }

        private string CreateClickCollapsibles(string componentTemplatePath, ModuleDefinition module)
        {
            string result = string.Empty;
            FilesBuilder fb = new FilesBuilder();

            string principalName = module.ComponentList.First<ComponentDefinition>(x => x.Principal).ComponentName.ToLower();


            foreach (var component in module.ComponentList)
            {
                if (!component.Principal)
                {
                    string LinkField = component.Binding.ForeignField;
                    Dictionary<string, string> patterns = new Dictionary<string, string>(){
                        {@"\${1:([^\}]*)\}", component.ComponentName.ToLower()},
                        {@"\${2:([^\}]*)\}", principalName},
                        {@"\${3:([^\}]*)\}", LinkField}
                    };

                    result = $"{result}{fb.BuildContent(componentTemplatePath, "clickcolapsibles", patterns)}";
                }
            }

            return result;
        }

        private string CreateDetailGroups(ObjectDefinition[] objectsList, string ComponentName, string htmlTemplatePath, ModuleDefinition module)
        {
            string result = string.Empty;

            List<string> groups = new List<string>();

            IEnumerable<ObjectDefinition> formFields = objectsList.Where<ObjectDefinition>(x => x.ObjectType.Equals("FormField", StringComparison.InvariantCultureIgnoreCase));

            foreach (var item in formFields)
            {
                if (!string.IsNullOrEmpty(item.Group) && !groups.Contains(item.Group))
                {
                    groups.Add(item.Group);

                    IEnumerable<ObjectDefinition> groupFields = formFields.Where<ObjectDefinition>(x => x.Group.Equals(item.Group, StringComparison.InvariantCultureIgnoreCase));

                    string controls = CreateDetailControls(groupFields.ToArray<ObjectDefinition>(), ComponentName, htmlTemplatePath);

                    Dictionary<string, string> patterns = new Dictionary<string, string>() {
                        {@"\${1:([^\}]*)\}", controls}//,
                    };

                    FilesBuilder fb = new FilesBuilder();

                    result = $"{result}\n\r{fb.BuildContent(htmlTemplatePath, "detail_group", patterns)}";

                }
            }

            Dictionary<string, string> patterns2 = new Dictionary<string, string>() {
                        {@"\${1:([^\}]*)\}", CreateChildComponents(htmlTemplatePath, module.ComponentList)}//,
                    };

            FilesBuilder fb2 = new FilesBuilder();
            string childComponents = $"\n\r{fb2.BuildContent(htmlTemplatePath, "child_components", patterns2)}";

            result = $"{result}{childComponents}";

            return result;
        }

        private string CreateChildComponents(string htmlTemplatePath, ComponentDefinition[] componentList)
        {
            FilesBuilder fb = new FilesBuilder();
            string childComponents = string.Empty;


            Dictionary<string, string> patterns = new Dictionary<string, string>();
            foreach (var component in componentList)
            {
                string poststrign = string.Empty;

                    patterns = new Dictionary<string, string>()
                    {
                        {@"\${1:([^\}]*)\}", component.ComponentName.ToLower()},
                        {@"\${2:([^\}]*)\}", CreateChildEvents(componentList)}
                    };

                    poststrign = $"{fb.BuildContent(htmlTemplatePath, "postchildcomponent", patterns)}";

                if (!component.Principal)
                {
                    patterns = new Dictionary<string, string>() {
                        {@"\${1:([^\}]*)\}", component.ComponentName.ToLower()},
                        {@"\${2:([^\}]*)\}", component.Texto != null ? component.Texto : string.Empty},
                        {@"\${3:([^\}]*)\}", component.Icono != null ? component.Icono : string.Empty},
                        {@"\${4:([^\}]*)\}", poststrign}
                };
                    childComponents = $"{childComponents}\n{fb.BuildContent(htmlTemplatePath, "child_component", patterns)}";
                }
            }

            return childComponents;
        }

        private static void CreateDirectory(string path)
        {
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
        }

        public void CreateDictionary(string dictionaryPath, string dictionaryKey, ObjectDefinition[] properties, Language language, ComponentDefinition job)
        {
            try
            {
                string dictionaryDirectory = Path.GetDirectoryName(dictionaryPath);
                CreateDirectory(dictionaryDirectory);

                if (!File.Exists(dictionaryPath))
                {
                    File.WriteAllText(dictionaryPath, "{}");
                }

                dictionaryKey = string.Format("lst{0}", dictionaryKey.ToLower());
                String content = File.ReadAllText(dictionaryPath);
                JObject objeto = JObject.Parse(content);
                string key = string.Empty;
                string value = string.Empty;
                string lines = string.Empty;

                foreach (var property in properties)
                {
                    //if (job.Principal)
                    //{
                    key = property.Name;
                    //}
                    //else
                    //{
                    //    key = $"{job.ComponentName.ToUpper()}{property.Name.ToUpper()}";
                    //}


                    switch (language)
                    {
                        case Language.Spanish:
                            value = property.Spanish;
                            break;
                        case Language.English:
                            value = property.English;
                            break;
                        default:
                            break;
                    }

                    lines = string.Format(@"{3} {2}{0}{2} : {2}{1}{2} {4}", key.ToUpper(), value, (char)34, lines, (char)44);
                }

                string valornuevo = string.Format("{0} {1} {2}", (char)123, lines, (char)125);
                if (objeto.Property(dictionaryKey) == null)
                    objeto.Add(dictionaryKey, "");

                objeto.Property(dictionaryKey).Value = JToken.Parse(valornuevo);
                string serialized = Newtonsoft.Json.JsonConvert.SerializeObject(objeto);

                File.WriteAllText(dictionaryPath, FormatJson(serialized));
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private string FormatJson(string str)
        {
            const string INDENT_STRING = "    ";

            var indent = 0;
            var quoted = false;
            var sb = new StringBuilder();
            for (var i = 0; i < str.Length; i++)
            {
                var ch = str[i];
                switch (ch)
                {
                    case '{':
                    case '[':
                        sb.Append(ch);
                        if (!quoted)
                        {
                            sb.AppendLine();
                            Enumerable.Range(0, ++indent).ForEach(item => sb.Append(INDENT_STRING));
                        }
                        break;
                    case '}':
                    case ']':
                        if (!quoted)
                        {
                            sb.AppendLine();
                            Enumerable.Range(0, --indent).ForEach(item => sb.Append(INDENT_STRING));
                        }
                        sb.Append(ch);
                        break;
                    case '"':
                        sb.Append(ch);
                        bool escaped = false;
                        var index = i;
                        while (index > 0 && str[--index] == '\\')
                            escaped = !escaped;
                        if (!escaped)
                            quoted = !quoted;
                        break;
                    case ',':
                        sb.Append(ch);
                        if (!quoted)
                        {
                            sb.AppendLine();
                            Enumerable.Range(0, indent).ForEach(item => sb.Append(INDENT_STRING));
                        }
                        break;
                    case ':':
                        sb.Append(ch);
                        if (!quoted)
                            sb.Append(" ");
                        break;
                    default:
                        sb.Append(ch);
                        break;
                }
            }
            return sb.ToString();
        }

        public ModuleReferenceDefinition CreateComponentReferences(ObjectDefinition[] fields, string templatePath, string jobName)
        {
            FilesBuilder fb = new FilesBuilder();
            List<ModuleReferenceDefinition> componentReferences = new List<ModuleReferenceDefinition>();
            ModuleReferenceDefinition result = new ModuleReferenceDefinition() { importModule = string.Empty, moduleName = string.Empty };
            result.functionsText = string.Empty;
            result.importComponent = string.Empty;
            result.viewchilds = string.Empty;
            result.onhidenpopup = string.Empty;
            result.onsetdefaulvalue = string.Empty;

            string currentTemplate = "";

            foreach (var item in fields)
            {
                Dictionary<string, string> patterns = new Dictionary<string, string>();
                patterns.Add(@"\${1:([^\}]*)\}", item.Name);

                if (item.ObjectType.Equals("FormField") && !string.IsNullOrEmpty(item.CellTemplate) && !item.CellTemplate.Equals(currentTemplate, StringComparison.InvariantCultureIgnoreCase))
                {
                    ModuleReferenceDefinition definition = fb.GetComponentReferences(templatePath, item.CellTemplate);
                    if (!string.IsNullOrEmpty(definition.importModule)
                        && !result.importModule.Contains(definition.importModule))
                    {
                        result.importModule = string.Format("{0}{1}{2}", result.importModule, definition.importModule, Environment.NewLine);
                    }

                    if (!string.IsNullOrEmpty(definition.moduleName)
                        && !result.moduleName.Contains(definition.moduleName))
                    {
                        result.moduleName = string.Format("{0}{1},{2}", result.moduleName, definition.moduleName, Environment.NewLine);
                    }

                    if (!string.IsNullOrEmpty(definition.importComponent)
                        && !result.importComponent.Contains(definition.importComponent))
                    {
                        result.importComponent = string.Format("{0}{1}{2}", result.importComponent, fb.Replace(definition.importComponent, patterns), Environment.NewLine);
                    }

                    if (!string.IsNullOrEmpty(definition.viewchilds)
                        && !result.viewchilds.Contains(definition.viewchilds))
                    {
                        result.viewchilds = string.Format("{0}{1}{2}", result.viewchilds, fb.Replace(definition.viewchilds, patterns), Environment.NewLine);
                    }

                    if (!string.IsNullOrEmpty(definition.onhidenpopup)
                        && !result.onhidenpopup.Contains(definition.onhidenpopup))
                    {
                        result.onhidenpopup = string.Format("{0}{1}{2}", result.onhidenpopup, fb.Replace(definition.onhidenpopup, patterns), Environment.NewLine);
                    }

                    if (!string.IsNullOrEmpty(definition.onsetdefaulvalue)
                        && !result.onsetdefaulvalue.Contains(definition.onsetdefaulvalue))
                    {
                        result.onsetdefaulvalue = string.Format("{0}{1}{2}", result.onsetdefaulvalue, fb.Replace(definition.onsetdefaulvalue, patterns), Environment.NewLine);
                    }

                    foreach (var functionLine in definition.functions)
                    {
                        result.functionsText = string.Format("{0}{1}{2}", result.functionsText, fb.Replace(functionLine, patterns), Environment.NewLine);
                    }
                }
            }

            return result;
        }

        private string GetDetailCriteria(ObjectDefinition[] fieldDefinition)
        {
            string result = string.Empty;
            foreach (var item in fieldDefinition)
            {
                if (item.DetailParameter)
                {
                    result = item.Name;
                }
            }
            return result;
        }

        public string CreateDefaultValueChildsComponents(ModuleDefinition module, string templatePath, string componentName)
        {
            string result = string.Empty;

            FilesBuilder fb = new FilesBuilder();

            foreach (var component in module.ComponentList)
            {
                //if (!component.Principal)
                if (!string.IsNullOrEmpty(component.Parent) && component.Parent.Equals(componentName, StringComparison.InvariantCultureIgnoreCase))
                {
                    Dictionary<string, string> patterns = new Dictionary<string, string>(){
                    {@"\${1:([^\}]*)\}", component.ComponentName},
                    {@"\${2:([^\}]*)\}", component.Get},
                    {@"\${code([^\}]*)\}", "${" + string.Format(@"this.editionData.{0}",component.Binding) + "}"}
                    };
                    result = string.Format("{0}{1}", fb.BuildContent(templatePath, "defaultValueChildComponents", patterns), result);
                }
            }

            return result.TrimStart(new char[] { '\r' }).TrimStart(new char[] { '\n' });
        }

        public string CreateViewChildsComponents(ModuleDefinition module, string templatePath, string componentName)
        {
            string result = string.Empty;

            FilesBuilder fb = new FilesBuilder();

            foreach (var component in module.ComponentList)
            {
                //if (!component.Principal)
                if (!string.IsNullOrEmpty(component.Parent) && component.Parent.Equals(componentName, StringComparison.InvariantCultureIgnoreCase))
                {
                    Dictionary<string, string> patterns = new Dictionary<string, string>(){
                    {@"\${1:([^\}]*)\}", component.ComponentName},
                    };
                    result = string.Format("{0}{1}", fb.BuildContent(templatePath, "viewChildComponents", patterns), result);
                }
            }

            return result.TrimStart(new char[] { '\r' }).TrimStart(new char[] { '\n' });
        }

        public string ImportChildsComponents(ModuleDefinition module, string templatePath, string componentName)
        {
            string result = string.Empty;

            FilesBuilder fb = new FilesBuilder();

            foreach (var component in module.ComponentList)
            {
                if (!string.IsNullOrEmpty(component.Parent) && component.Parent.Equals(componentName, StringComparison.InvariantCultureIgnoreCase))
                {
                    Dictionary<string, string> patterns = new Dictionary<string, string>(){
                    {@"\${1:([^\}]*)\}", component.ComponentName},
                    {@"\${2:([^\}]*)\}", component.ComponentName.ToLower()},
                    };
                    result = string.Format("{0}{1}", fb.BuildContent(templatePath, "importChildComponents", patterns), result);
                }
            }

            return result.TrimStart(new char[] { '\r' }).TrimStart(new char[] { '\n' });
        }

        public string CreateSetPreLoadFieldFromParent(ModuleDefinition module, string templatePath, string componentName)
        {
            string result = string.Empty;

            FilesBuilder fb = new FilesBuilder();

            foreach (var component in module.ComponentList)
            {
                if (!string.IsNullOrEmpty(component.Parent) && component.Parent.Equals(componentName, StringComparison.InvariantCultureIgnoreCase))
                {
                    foreach (var myObject in component.ObjectsList)
                    {
                        if (myObject.Preload != null && myObject.Preload.Exists(x => x.Name.Equals("source") && x.Value.Equals("parent")))
                        {
                            Dictionary<string, string> patterns = new Dictionary<string, string>(){
                            {@"\${1:([^\}]*)\}", component.ComponentName},
                            {@"\${2:([^\}]*)\}", myObject.Name},
                            {@"\${3:([^\}]*)\}", myObject.Preload.Find(x => x.Name.Equals("Field")).Value},
                            };
                            result = string.Format("{0}{1}", fb.BuildContent(templatePath, "setPreLoadFieldFromParent", patterns), result);
                        }
                    }
                }
            }

            return result.TrimStart(new char[] { '\r' }).TrimStart(new char[] { '\n' });
        }

        public string CreatePreLoadVariablesFromParent(ModuleDefinition module, string templatePath, string componentName)
        {
            string result = string.Empty;

            FilesBuilder fb = new FilesBuilder();

            foreach (var component in module.ComponentList)
            {
                if (component.ComponentName.Equals(componentName, StringComparison.InvariantCultureIgnoreCase))
                {
                    foreach (var myObject in component.ObjectsList)
                    {
                        if (myObject.Preload != null && myObject.Preload.Exists(x => x.Name.Equals("source") && x.Value.Equals("parent")))
                        {
                            Dictionary<string, string> patterns = new Dictionary<string, string>(){
                            {@"\${1:([^\}]*)\}", myObject.Name}
                            ,{@"\${2:([^\}]*)\}", homologateTypes(myObject.Type)},
                            };
                            result = string.Format("{0}{1}", fb.BuildContent(templatePath, "preLoadVariablesFromParent", patterns), result);
                        }
                    }
                }
            }

            return result.TrimStart(new char[] { '\r' }).TrimStart(new char[] { '\n' });
        }

        public string CreateSetPreLoadToEditionDataFromParent(ModuleDefinition module, string templatePath, string componentName)
        {
            string result = string.Empty;

            FilesBuilder fb = new FilesBuilder();

            foreach (var component in module.ComponentList)
            {
                if (component.ComponentName.Equals(componentName, StringComparison.InvariantCultureIgnoreCase))
                {
                    foreach (var myObject in component.ObjectsList)
                    {
                        if (myObject.Preload != null && myObject.Preload.Exists(x => x.Name.Equals("source") && x.Value.Equals("parent")))
                        {
                            Dictionary<string, string> patterns = new Dictionary<string, string>(){
                            {@"\${1:([^\}]*)\}", myObject.Name}
                            };
                            result = string.Format("{0}{1}", fb.BuildContent(templatePath, "setPreLoadToEditionDataFromParent", patterns), result);
                        }
                    }
                }
            }

            return result.TrimStart(new char[] { '\r' }).TrimStart(new char[] { '\n' });
        }

        private string CreateContextMenuItemsCollection(string componentTemplatePath, string componentName, ModuleDefinition module)
        {
            FilesBuilder fb = new FilesBuilder();
            string contextMenu = string.Empty;

            ComponentDefinition component = module.ComponentList.First<ComponentDefinition>(x => x.ComponentName.Equals(componentName, StringComparison.InvariantCultureIgnoreCase));

            int count = component.ObjectsList.ToList<ObjectDefinition>().FindAll(x => x.ObjectType.Equals("MenuItem", StringComparison.InvariantCultureIgnoreCase)).Count;

            int index = 0;
            if (count > 0)
            {
                List<ObjectDefinition> menuItems = component.ObjectsList.ToList<ObjectDefinition>().FindAll(x => x.ObjectType.Equals("MenuItem", StringComparison.InvariantCultureIgnoreCase));

                StringBuilder sbItems = new StringBuilder();
                foreach (ObjectDefinition menuItem in menuItems)
                {
                    index++;
                    Dictionary<string, string> patterns = new Dictionary<string, string>(){
                        {@"\${1:([^\}]*)\}", menuItem.Spanish},
                        {@"\${2:([^\}]*)\}", index.ToString()}
                    };

                    string newContextMenu = fb.BuildContent(componentTemplatePath, "menuItem", patterns);
                    sbItems.AppendLine(newContextMenu);
                }

                StringBuilder sb = new StringBuilder();
                sb.AppendLine("this.contextItems = [");
                sb.AppendLine(sbItems.ToString());
                sb.AppendLine("];");

                contextMenu = sb.ToString();
            }

            return contextMenu;
        }

        private string CreateMenuItemsVariable(string componentTemplatePath, string componentName, ModuleDefinition module)
        {
            FilesBuilder fb = new FilesBuilder();
            string contextMenu = string.Empty;

            ComponentDefinition component = module.ComponentList.First<ComponentDefinition>(x => x.ComponentName.Equals(componentName, StringComparison.InvariantCultureIgnoreCase));

            int count = component.ObjectsList.ToList<ObjectDefinition>().FindAll(x => x.ObjectType.Equals("MenuItem", StringComparison.InvariantCultureIgnoreCase)).Count;

            if (count > 0)
            {
                contextMenu = "contextItems: any[]";
            }

            return contextMenu;
        }

        private string CreateModelLines(ObjectDefinition[] modelProperties, string objectType)
        {
            string properties = string.Empty;

            foreach (var item in modelProperties)
            {
                string modelType = string.Empty;
                if (item.ObjectType.Equals(objectType))
                {
                    modelType = homologateTypes(item.Type);
                    properties = string.Format("{0} {1}        {2}:{3};", properties, Environment.NewLine, item.Name, modelType);
                }

            }

            return properties;
        }

        private string homologateTypes(string itemType)
        {
            string homologation = string.Empty;

            if (itemType.ToLower().Contains("date"))
            {
                homologation = "Date";
            }
            else
            {
                if (itemType.ToLower().Contains("char") || itemType.ToLower().Contains("string"))
                {
                    homologation = "string";
                }
                else
                {
                    if (itemType.ToLower().Contains("boolean"))
                    {
                        homologation = "boolean";
                    }
                    else
                    {
                        homologation = "Number";
                    }
                }
            }

            return homologation;
        }

        private static string CreateAgGridColumnLines(ObjectDefinition[] modelProperties, string componentName, string rendererTemplatePath)
        {
            try
            {
                Dictionary<string, string> pattern = new Dictionary<string, string>();
                FilesBuilder fb = new FilesBuilder();

                string columnsLines = string.Empty;

                foreach (var item in modelProperties)
                {
                    string cellTemplate = string.Empty;
                    string width = string.Empty;
                    string dateFormatt = string.Empty;

                    if (item.ObjectType.Equals("DeleteColumn"))
                    {
                        pattern = new Dictionary<string, string>()
                        {
                            { @"\${1:([^\}]*)\}", item.Name}
                        };

                        columnsLines = $"{fb.BuildContent(rendererTemplatePath, "childdeletebutton", pattern)}";
                    }

                    if (item.ObjectType.Equals("EditColumn"))
                    {
                        pattern = new Dictionary<string, string>()
                        {
                            { @"\${1:([^\}]*)\}", item.Name}
                        };

                        columnsLines = $"{columnsLines}\r\n{fb.BuildContent(rendererTemplatePath, "childeditbutton", pattern)}";
                    }

                    if (item.ObjectType.Equals("ListColumn"))
                    {
                        if (!string.IsNullOrEmpty(item.CellTemplate))
                        {
                            switch (item.CellTemplate.ToLower())
                            {
                                case "celltemplatecode":
                                    cellTemplate = "cellTemplate=\"cellTemplateCode\" ";
                                    break;
                                default:
                                    cellTemplate = string.Empty;
                                    break;
                            }
                        }

                        string cellRenderer = string.Empty;
                        if (item.CellRenderer != null && item.CellRenderer.Count > 0)
                        {
                            
                            string cellrendererContent = string.Empty;
                            foreach (var cellrenderer in item.CellRenderer)
                            {
                                pattern = new Dictionary<string, string>(){
                                    { @"\${1:([^\}]*)\}", item.Type.Equals("number") ? cellrenderer.Value : $"\"{cellrenderer.Value}\""},
                                    { @"\${2:([^\}]*)\}", cellrenderer.Icon},
                                    { @"\${3:([^\}]*)\}", cellrenderer.Color},
                                    { @"\${4:([^\}]*)\}", cellrenderer.Text.Equals("params.value", StringComparison.InvariantCultureIgnoreCase) ? "${params.value}" : cellrenderer.Text}
                                };

                                if (cellrenderer.Value.Equals("Default", StringComparison.InvariantCultureIgnoreCase))
                                {
                                    cellrendererContent = $"{cellrendererContent}\n\t{fb.BuildContent(rendererTemplatePath, "cellrendereroptiondefault", pattern)}";
                                }
                                else
                                {
                                    cellrendererContent = $"{cellrendererContent}\n\t{fb.BuildContent(rendererTemplatePath, "cellrendereroption", pattern)}";
                                }

                            }

                            pattern = new Dictionary<string, string>() { { @"\${1:([^\}]*)\}", cellrendererContent } };

                            cellRenderer = fb.BuildContent(rendererTemplatePath, "cellrenderer", pattern);
                            cellRenderer = $",{cellRenderer}";
                        }


                        if (item.Width > 0)
                        {
                            width = $", minWidth: {item.Width}, maxWidth: {item.Width}";
                        }

                        if (item.Type.ToLower().Contains("date"))
                        {
                            dateFormatt = " dataType=\"datetime\" format=\"dd/MM/yyyy HH:mm\" ";
                        }

                        columnsLines = $"{columnsLines}{{ headerName: '{item.Spanish}', field: \"{item.Name}\", sortable: true, filter: \"agTextColumnFilter\" , enablePivot: true , enableRowGroup: true{width}{cellRenderer} }},{Environment.NewLine}";
                    }
                }

                return columnsLines;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private string CreateDetailControls(ObjectDefinition[] objectDefinitions, string componentName, string htmlTemplatePath)
        {

            string result = string.Empty;

            Dictionary<string, string> patterns = new Dictionary<string, string>();
            FilesBuilder fb = new FilesBuilder();
            string controls = string.Empty;

            foreach (var control in objectDefinitions.Where<ObjectDefinition>(x => x.ObjectType.Equals("FormField", StringComparison.InvariantCultureIgnoreCase)))
            {

                patterns = new Dictionary<string, string>();

                if (string.IsNullOrEmpty(control.CellTemplate))
                {
                    patterns.Add(@"\${1:([^\}]*)\}", control.Name);
                    if (!string.IsNullOrEmpty(control.BehaviorInEdition) && control.BehaviorInEdition.Equals("disabled", StringComparison.InvariantCultureIgnoreCase))
                    {
                        patterns.Add(@"\${2:([^\}]*)\}", "[disabled]=\"!newRow\"");
                    }
                    string field = fb.BuildContent(htmlTemplatePath, $"detail_{control.Type}_field", patterns);

                    patterns = new Dictionary<string, string>();
                    patterns.Add(@"\${1:([^\}]*)\}", componentName);
                    patterns.Add(@"\${2:([^\}]*)\}", control.Name.ToUpper());
                    patterns.Add(@"\${3:([^\}]*)\}", field);
                    controls = $"{controls}\n\r{fb.BuildContent(htmlTemplatePath, "detail_control", patterns)}";
                }
                else
                {

                    if (control.CellTemplate.Equals("GenericAutoComplete", StringComparison.InvariantCultureIgnoreCase))
                    {
                        patterns.Add(@"\${1:([^\}]*)\}", $"{{{{\"lst{componentName.ToLower()}.{control.Name.ToUpper()}\" | translate }}}}");
                        patterns.Add(@"\${2:([^\}]*)\}", control.Name);
                        patterns.Add(@"\${3:([^\}]*)\}", control.TemplateParameters.FirstOrDefault(x => x.Name.Equals("columnaCodigo", StringComparison.InvariantCultureIgnoreCase)).Value);
                        patterns.Add(@"\${4:([^\}]*)\}", control.TemplateParameters.FirstOrDefault(x => x.Name.Equals("columnaNombre", StringComparison.InvariantCultureIgnoreCase)).Value);
                        patterns.Add(@"\${5:([^\}]*)\}", control.TemplateParameters.FirstOrDefault(x => x.Name.Equals("controlador", StringComparison.InvariantCultureIgnoreCase)).Value);
                        patterns.Add(@"\${6:([^\}]*)\}", control.TemplateParameters.FirstOrDefault(x => x.Name.Equals("columnaFiltro", StringComparison.InvariantCultureIgnoreCase)).Value);
                        patterns.Add(@"\${7:([^\}]*)\}", control.TemplateParameters.FirstOrDefault(x => x.Name.Equals("filtros", StringComparison.InvariantCultureIgnoreCase)).Value);

                        controls = $"{controls}\n\r{fb.BuildContent(htmlTemplatePath, "detail_generic_autocomplete", patterns)}";
                    }

                    if (control.CellTemplate.Equals("GenericSelectBox", StringComparison.InvariantCultureIgnoreCase))
                    {
                        patterns.Add(@"\${1:([^\}]*)\}", $"{{{{\"lst{componentName.ToLower()}.{control.Name.ToUpper()}\" | translate }}}}");
                        patterns.Add(@"\${2:([^\}]*)\}", control.Name);
                        patterns.Add(@"\${3:([^\}]*)\}", control.TemplateParameters.FirstOrDefault(x => x.Name.Equals("columnaCodigo", StringComparison.InvariantCultureIgnoreCase)).Value);
                        patterns.Add(@"\${4:([^\}]*)\}", control.TemplateParameters.FirstOrDefault(x => x.Name.Equals("columnaNombre", StringComparison.InvariantCultureIgnoreCase)).Value);
                        patterns.Add(@"\${5:([^\}]*)\}", control.TemplateParameters.FirstOrDefault(x => x.Name.Equals("controlador", StringComparison.InvariantCultureIgnoreCase)).Value);
                        patterns.Add(@"\${6:([^\}]*)\}", control.TemplateParameters.FirstOrDefault(x => x.Name.Equals("columnaFiltro", StringComparison.InvariantCultureIgnoreCase)).Value);
                        patterns.Add(@"\${7:([^\}]*)\}", control.TemplateParameters.FirstOrDefault(x => x.Name.Equals("filtros", StringComparison.InvariantCultureIgnoreCase)).Value);

                        controls = $"{controls}\n\r{fb.BuildContent(htmlTemplatePath, "detail_generic_selectbox", patterns)}";
                    }

                }


            }

            return controls;
        }

        private string CreateField(ObjectDefinition control)
        {


            string result = string.Empty;



            return result;
        }

        public string ToUpperFirstLetter(string source)
        {
            if (string.IsNullOrEmpty(source))
                return string.Empty;
            // convert to char array of the string
            char[] letters = source.ToCharArray();
            // upper case the first char
            letters[0] = char.ToUpper(letters[0]);
            // return the array made of the new char array
            return new string(letters);
        }
    }
}
