﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TemplateManager
{
    //public class ComponentBuilder
    //{
    //    string routingappTemplatePath = @"C:\Users\Admin\AppData\Roaming\Code\User\snippets\typescript.json";
    //    string scssTemplatePath = @"C:\Users\Admin\AppData\Roaming\Code\User\snippets\typescript.json";
    //    string componentTemplatePath = @"C:\Users\Admin\AppData\Roaming\Code\User\snippets\typescript.json";
    //    string componentSpecTemplatePath = @"C:\Users\Admin\AppData\Roaming\Code\User\snippets\typescript.json";
    //    string modelTemplatePath = @"C:\Users\Admin\AppData\Roaming\Code\User\snippets\typescript.json";
    //    string htmlTemplatePath = @"C:\Users\Admin\AppData\Roaming\Code\User\snippets\html.json";
    //    string serviceTemplatePath = @"C:\Users\Admin\AppData\Roaming\Code\User\snippets\typescript.json";
    //    string routingTemplatePath = @"C:\Users\Admin\AppData\Roaming\Code\User\snippets\typescript.json";
    //    string moduleTemplatePath = @"C:\Users\Admin\AppData\Roaming\Code\User\snippets\typescript.json";
    //    string moduleSpecTemplatePath = @"C:\Users\Admin\AppData\Roaming\Code\User\snippets\typescript.json";

    //    ComponentDefinition component = new ComponentDefinition();

    //    public ComponentBuilder()
    //    {
    //        component.componentProperties = new Dictionary<string, string>();
    //        component.componentProperties.Add("Name", "juan");

    //        string Name = component.componentProperties["Name"];

    //        string modulePath = string.Format(@"D:\argos\ui-tms\src\app\pages\list\{0}\", Name.ToLower());
    //        string servicePath = string.Format(@"D:\argos\ui-tms\src\app\services\{0}\", Name.ToLower());
    //        string modelPath = @"D:\argos\ui-tms\src\app\models\baseModels\";
    //        string componentPath = string.Format(@"{0}\lst{1}\", modulePath, Name.ToLower());
    //        string routingPath = @"D:\argos\ui-tms\src\app\app.routing.ts";

    //        component.ObjectsList = new FieldDefinition[] {
    //             new FieldDefinition(){ Name = "WayId", Type = "string" }
    //            ,new FieldDefinition(){ Name = "WayNumber", Type = "string" }
    //            ,new FieldDefinition(){ Name = "WayDate", Type = "Date" }
    //            ,new FieldDefinition(){ Name = "StaName", Type = "string" }
    //            ,new FieldDefinition(){ Name = "WayCustomer_UcrName", Type = "string" }
    //            ,new FieldDefinition(){ Name = "DepartureCitCode", Type = "string" }
    //            ,new FieldDefinition(){ Name = "ArrivalCitCode", Type = "string" }
    //        };

    //        FieldDefinition[] myfields = new FieldDefinition[] {
    //             new FieldDefinition(){ Name = "WayId", Type = "string" }
    //            ,new FieldDefinition(){ Name = "WayNumber", Type = "string" }
    //            ,new FieldDefinition(){ Name = "WayDate", Type = "Date" }
    //            ,new FieldDefinition(){ Name = "StaName", Type = "string" }
    //            ,new FieldDefinition(){ Name = "WayCustomer_UcrName", Type = "string" }
    //            ,new FieldDefinition(){ Name = "DepartureCitCode", Type = "string" }
    //            ,new FieldDefinition(){ Name = "ArrivalCitCode", Type = "string" }
    //        };

    //        ComponentDefinition work = new ComponentDefinition()
    //        {
    //            ComponentName = "NuevoComponente",
    //            Fields = new FieldDefinition[] {
    //                 new FieldDefinition(){ Name = "WayId", Type = "string" }
    //                ,new FieldDefinition(){ Name = "WayNumber", Type = "string" }
    //                ,new FieldDefinition(){ Name = "WayDate", Type = "Date" }
    //                ,new FieldDefinition(){ Name = "StaName", Type = "string" }
    //                ,new FieldDefinition(){ Name = "WayCustomer_UcrName", Type = "string" }
    //                ,new FieldDefinition(){ Name = "DepartureCitCode", Type = "string" }
    //                ,new FieldDefinition(){ Name = "ArrivalCitCode", Type = "string" }
    //            }
    //        };


    //        ComponentDefinition work2 = new ComponentDefinition()
    //        {
    //            ComponentName = "NuevoComponente",
    //            Fields = new FieldDefinition[] {
    //                 new FieldDefinition(){ Name = "WayId", Type = "string" }
    //                ,new FieldDefinition(){ Name = "WayNumber", Type = "string" }
    //                ,new FieldDefinition(){ Name = "WayDate", Type = "Date" }
    //                ,new FieldDefinition(){ Name = "StaName", Type = "string" }
    //                ,new FieldDefinition(){ Name = "WayCustomer_UcrName", Type = "string" }
    //                ,new FieldDefinition(){ Name = "DepartureCitCode", Type = "string" }
    //                ,new FieldDefinition(){ Name = "ArrivalCitCode", Type = "string" }
    //            }
    //        };

    //        List<ComponentDefinition> myWorks = new List<ComponentDefinition>();
    //        myWorks.Add(work);
    //        myWorks.Add(work2);

    //        string serialized = Newtonsoft.Json.JsonConvert.SerializeObject(myWorks);
    //        //WorkDefinition newWork = Newtonsoft.Json.JsonConvert.DeserializeObject<WorkDefinition>(serialized);


    //        component.Files = new List<ActionDefinition>(){

    //            new ActionDefinition(){
    //                ActionType = ActionTypeEnum.ModifyFile,
    //                TemplateName = "routingapp", 
    //                FilePath = routingPath, 
    //                FileTemplatePath = scssTemplatePath,
    //                Patterns = new Dictionary<string,TokenDefinition>(){
    //                    { @"\${1:([^\}]*)\}", new TokenDefinition(){ TransformationType = TransformationTypeEnum.ToLower, Pattern = "", ValueType = ValueTypeEnum.Property, AditionalInfo = "Name"}},
    //                    { @"\${2:([^\}]*)\}", new TokenDefinition(){ TransformationType = TransformationTypeEnum.None, Pattern = "", ValueType = ValueTypeEnum.Property, AditionalInfo = "Name"}}
    //                }
    //            },

    //            new ActionDefinition(){
    //                ActionType = ActionTypeEnum.NewFile,
    //                TemplateName = "scss", 
    //                FilePath = string.Format(@"{0}lst{1}.component.scss", componentPath, component.componentProperties["Name"].ToLower()), 
    //                FileTemplatePath = scssTemplatePath,
    //                Patterns = new Dictionary<string,TokenDefinition>()
    //            },

    //            new ActionDefinition(){
    //                ActionType = ActionTypeEnum.NewFile,
    //                TemplateName = "componente", 
    //                FilePath = string.Format(@"{0}lst{1}.component.ts", componentPath, Name.ToLower()), 
    //                FileTemplatePath = routingappTemplatePath,
    //                Patterns = new Dictionary<string,TokenDefinition>(){
    //                    { @"\${1:([^\}]*)\}", new TokenDefinition(){ TransformationType = TransformationTypeEnum.ToLower, Pattern = "", ValueType = ValueTypeEnum.Property, AditionalInfo = "Name"}},
    //                    { @"\${2:([^\}]*)\}", new TokenDefinition(){ TransformationType = TransformationTypeEnum.FormattValues, Pattern = "Lst{0}", ValueType = ValueTypeEnum.Property, AditionalInfo = "Name"}},
    //                    { @"\${3:([^\}]*)\}", new TokenDefinition(){ TransformationType = TransformationTypeEnum.FormattValues, Pattern = "srv{0}", ValueType = ValueTypeEnum.Property, AditionalInfo = "Name"}},
    //                    { @"\${4:([^\}]*)\}", new TokenDefinition(){ TransformationType = TransformationTypeEnum.FormattValues, Pattern = "{0}Service", ValueType = ValueTypeEnum.Property, AditionalInfo = "Name"}},
    //                    { @"\${5:([^\}]*)\}", new TokenDefinition(){ TransformationType = TransformationTypeEnum.None, Pattern = "", ValueType = ValueTypeEnum.Property, AditionalInfo = "Name"}},
    //                    { @"\${6:([^\}]*)\}", new TokenDefinition(){ TransformationType = TransformationTypeEnum.None, Pattern = "", ValueType = ValueTypeEnum.SingleValue, AditionalInfo = ".EightRow2Col_wi
    //                    h"}},
    //                    { @"\${7:([^\}]*)\}", new TokenDefinition(){ TransformationType = TransformationTypeEnum.None, Pattern = "", ValueType = ValueTypeEnum.SingleValue, AditionalInfo = ".EightRow2Col_heigh"}},
    //                    { @"\${8:([^\}]*)\}", new TokenDefinition(){ TransformationType = TransformationTypeEnum.FormattValues, Pattern = "{0}Model", ValueType = ValueTypeEnum.Property, AditionalInfo = "Name"}}
    //                }
    //            },

    //            new ActionDefinition(){
    //                ActionType = ActionTypeEnum.NewFile,
    //                TemplateName = "component_spec", 
    //                FilePath = string.Format(@"{0}\lst{1}.component.spec.ts", componentPath, Name.ToLower()), 
    //                FileTemplatePath = componentSpecTemplatePath,
    //                Patterns = new Dictionary<string,TokenDefinition>(){
    //                    { @"\${1:([^\}]*)\}", new TokenDefinition(){ TransformationType = TransformationTypeEnum.None, Pattern = "", ValueType = ValueTypeEnum.Property, AditionalInfo = "Name"}},
    //                    { @"\${2:([^\}]*)\}", new TokenDefinition(){ TransformationType = TransformationTypeEnum.ToLower, Pattern = "", ValueType = ValueTypeEnum.Property, AditionalInfo = "Name"}}
    //                }
    //            },

    //            new ActionDefinition(){
    //                ActionType = ActionTypeEnum.NewFile,
    //                TemplateName = "modelo", 
    //                FilePath = string.Format(@"{0}{1}.model.ts", modelPath, Name.ToLower()), 
    //                FileTemplatePath = modelTemplatePath,
    //                Patterns = new Dictionary<string,TokenDefinition>(){
    //                    { @"\${1:([^\}]*)\}", new TokenDefinition(){ TransformationType = TransformationTypeEnum.None, Pattern = "", ValueType = ValueTypeEnum.Property, AditionalInfo = "Name"}},
    //                    { @"\${2:([^\}]*)\}", new TokenDefinition(){ TransformationType = TransformationTypeEnum.ToLower, Pattern = "", ValueType = ValueTypeEnum.Field, AditionalInfo = "Name,Type"}}
    //                }
    //            },

    //            new ActionDefinition(){
    //                ActionType = ActionTypeEnum.NewFile,
    //                TemplateName = "Html_list", 
    //                FilePath = string.Format(@"{0}lst{1}.component.html", modelPath, Name.ToLower()), 
    //                FileTemplatePath = modelTemplatePath,
    //                Patterns = new Dictionary<string,TokenDefinition>(){
    //                    { @"\${1:([^\}]*)\}", new TokenDefinition(){ TransformationType = TransformationTypeEnum.None, Pattern = "", ValueType = ValueTypeEnum.Property, AditionalInfo = "Name"}},
    //                    { @"\${2:([^\}]*)\}", new TokenDefinition(){ TransformationType = TransformationTypeEnum.ToLower, Pattern = "", ValueType = ValueTypeEnum.Field, AditionalInfo = "Name,Type"}}
    //                }
    //            },

    //            new ActionDefinition(){
    //                ActionType = ActionTypeEnum.NewFile,
    //                TemplateName = "servicio", 
    //                FilePath = string.Format("{0}{1}.service.ts", servicePath, Name.ToLower()), 
    //                FileTemplatePath = modelTemplatePath,
    //                Patterns = new Dictionary<string,TokenDefinition>(){
    //                    { @"\${1:([^\}]*)\}", new TokenDefinition(){ TransformationType = TransformationTypeEnum.None, Pattern = "", ValueType = ValueTypeEnum.Property, AditionalInfo = "Name"}},
    //                    { @"\${2:([^\}]*)\}", new TokenDefinition(){ TransformationType = TransformationTypeEnum.ToLower, Pattern = "", ValueType = ValueTypeEnum.Property, AditionalInfo = "Name"}}
    //                }
    //            },

    //            new ActionDefinition(){
    //                ActionType = ActionTypeEnum.NewFile,
    //                TemplateName = "routing", 
    //                FilePath = string.Format(@"{0}{1}.routing.module.ts", modulePath, Name.ToLower()), 
    //                FileTemplatePath = modelTemplatePath,
    //                Patterns = new Dictionary<string,TokenDefinition>(){
    //                    { @"\${1:([^\}]*)\}", new TokenDefinition(){ TransformationType = TransformationTypeEnum.None, Pattern = "", ValueType = ValueTypeEnum.Property, AditionalInfo = "Name"}},
    //                    { @"\${2:([^\}]*)\}", new TokenDefinition(){ TransformationType = TransformationTypeEnum.ToLower, Pattern = "", ValueType = ValueTypeEnum.Property, AditionalInfo = "Name"}}
    //                }
    //            },

    //            new ActionDefinition(){
    //                ActionType = ActionTypeEnum.NewFile,
    //                TemplateName = "modulo", 
    //                FilePath = string.Format(@"{0}{1}.module.ts", modulePath, Name.ToLower()), 
    //                FileTemplatePath = modelTemplatePath,
    //                Patterns = new Dictionary<string,TokenDefinition>(){
    //                    { @"\${1:([^\}]*)\}", new TokenDefinition(){ TransformationType = TransformationTypeEnum.FormattValues, Pattern = "{0}RoutingModule", ValueType = ValueTypeEnum.Property, AditionalInfo = "Name"}},
    //                    { @"\${2:([^\}]*)\}", new TokenDefinition(){ TransformationType = TransformationTypeEnum.None, Pattern = "", ValueType = ValueTypeEnum.Property, AditionalInfo = "Name"}},
    //                    { @"\${3:([^\}]*)\}", new TokenDefinition(){ TransformationType = TransformationTypeEnum.None, Pattern = "", ValueType = ValueTypeEnum.Property, AditionalInfo = "Name"}},
    //                    { @"\${4:([^\}]*)\}", new TokenDefinition(){ TransformationType = TransformationTypeEnum.FormattValues, Pattern = "{0}Module", ValueType = ValueTypeEnum.Property, AditionalInfo = "Name"}},
    //                    { @"\${5:([^\}]*)\}", new TokenDefinition(){ TransformationType = TransformationTypeEnum.ToLower, Pattern = "", ValueType = ValueTypeEnum.Property, AditionalInfo = "Name"}}
    //                }
    //            },

    //            new ActionDefinition(){
    //                ActionType = ActionTypeEnum.NewFile,
    //                TemplateName = "module_spec", 
    //                FilePath = string.Format(@"{0}{1}.module.spec.ts", modulePath, Name.ToLower()), 
    //                FileTemplatePath = modelTemplatePath,
    //                Patterns = new Dictionary<string,TokenDefinition>(){
    //                    { @"\${1:([^\}]*)\}", new TokenDefinition(){ TransformationType = TransformationTypeEnum.None, Pattern = "", ValueType = ValueTypeEnum.Property, AditionalInfo = "Name"}},
    //                    { @"\${2:([^\}]*)\}", new TokenDefinition(){ TransformationType = TransformationTypeEnum.None, Pattern = "", ValueType = ValueTypeEnum.Property, AditionalInfo = "Name"}}
    //                }
    //            },

    //        };
    //    }
    //}
}
