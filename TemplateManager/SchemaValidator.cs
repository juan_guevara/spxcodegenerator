﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TemplateManager
{
    public static class SchemaValidator
    {
        public static void Validate(string workDefinitionPath) {

            string schemaPath = System.Configuration.ConfigurationManager.AppSettings.Get("schemaPath");

            TemplateValidation validator = new TemplateValidation();
            ValidateRequest validationRequest = new ValidateRequest();

            String workDefinition = File.ReadAllText(workDefinitionPath);
            String schema = File.ReadAllText(schemaPath);

            validationRequest.Json = workDefinition;
            validationRequest.Schema = schema;

            ValidateResponse validationResult = validator.Validate(validationRequest);

            if (!validationResult.Valid)
            {
                string errors = string.Join("\r\n", validationResult.Errors.Select(t => $"{t.Message} {t.Path} {t.Schema.Description} " +
                $"{ string.Join("\r\n", t.ChildErrors.Select(y => $"{y.Message} {y.Path} {y.Schema.Description} {string.Join("\r\n", y.ChildErrors.Select(z => $"{z.Message} {z.Path} {z.Schema.Description}").ToArray())}").ToArray())}").ToArray());
                throw new Exception(errors);
            }

        }
    }
}
