﻿using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Schema;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TemplateManager
{
    public class ValidateResponse
    {
        public bool Valid { get; set; }
        public IList<ValidationError> Errors { get; set; }
    }

    public class ValidateRequest
    {
        public string Json { get; set; }
        public string Schema { get; set; }
    }

    public class TemplateValidation
    {
        public ValidateResponse Validate(ValidateRequest request) {

            var jsonSchema = JSchema.Parse(request.Schema);
            JToken json = JToken.Parse(request.Json);

            IList<ValidationError> errors;
            bool valid = json.IsValid(jsonSchema, out errors);

            // return error messages and line info to the browser
            return new ValidateResponse
            {
                Valid = valid,
                Errors = errors
            };
       }
    }
}
