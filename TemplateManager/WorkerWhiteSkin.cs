﻿using System.Collections.Generic;
using System.Linq;

namespace TemplateManager
{
    public class WorkerWhiteSkin
    {
        private ModuleManager moduleManager = new ModuleManager();
        private ComponentManager componentManager = new ComponentManager();

        public void ExecuteWork(string workDefinitionPath, Dictionary<string, string> paths)
        {
            //SchemaValidator.Validate(workDefinitionPath);

            List<ModuleDefinition> modules = WorkReader.Read(workDefinitionPath);
            List<string> componentsNames = new List<string>();

            foreach (var module in modules)
            {
                foreach (var component in module.ComponentList)
                {
                    if (component.Principal) {
                        componentsNames.Add(component.ComponentName);
                    }
                }
            }

            foreach (var module in modules)
            {
                List<ComponentDefinition> components = module.ComponentList.ToList<ComponentDefinition>();

                ComponentDefinition principal = components.First<ComponentDefinition>(x => x.Principal);

                componentManager.CreatePrincipalComponent(principal, module, componentsNames, paths);

                foreach (var component in components)
                {
                    if (!component.Principal)
                    {
                        componentManager.CreateComponent(component, module, paths, principal.ComponentName);
                    }
                }

                //moduleManager.RegisterModule(principal, module, modules);
                moduleManager.RegisterModule(principal, module);
            }
        }
    }
}
