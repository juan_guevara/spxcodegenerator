﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Text.RegularExpressions;

namespace TemplateManager
{
    public class Template
    {
        public string prefix { get; set; }
        public string[] body { get; set; }
        public string description { get; set; }
        public string importModule { get; set; }
        public string importComponent { get; set; }
        public string moduleName { get; set; }
        public string viewchilds { get; set; }
        public string[] functions { get; set; }
        public string onhidenpopup { get; set; }
        public string onsetdefaulvalue { get; set; }
    }

    //public class Modules { 
    //    public string ModuleName { get; set; }
    //    //public ComponentCollection ComponentList { get; set; }
    //    //ComponentCollection ComponentList { get; set; }
    //    public ComponentDefinition[] ComponentsList { get; set; }
    //}

    //public class ModuleCollection
    //{
    //    public ModuleDefinition[] ModuleList { get; set; }
    //}

    public class ComponentCollection
    {
        public ComponentDefinition[] ComponentsList { get; set; }
    }

    public class FieldsCollection
    {
        public ObjectDefinition[] ListFields { get; set; }
    }

    public class FilesBuilder
    {
        public string BuildContent(string templatePath, string TemplateName, Dictionary<string, string> patterns)
        {
            string result = string.Empty;
            String content = File.ReadAllText(templatePath);
            var objeto = JObject.Parse(content);

            foreach (var item in objeto)
            {
                if (item.Key.Equals(TemplateName))
                {
                    Template valor = item.Value.ToObject<Template>();
                    foreach (var line in valor.body)
                    {
                        string replacedLine = Replace(line, patterns);
                        if (!string.IsNullOrEmpty(replacedLine))
                        {
                            result = string.Format("{0} {1} {2}", result, Environment.NewLine, replacedLine);
                        }
                    }
                }
            }

            return result;
        }

        public ModuleReferenceDefinition GetComponentReferences(string templatePath, string TemplateName)
        {
            String content = File.ReadAllText(templatePath);
            var objeto = JObject.Parse(content);
            ModuleReferenceDefinition result = new ModuleReferenceDefinition();

            foreach (var item in objeto)
            {
                if (item.Key.Equals(TemplateName))
                {
                    Template valor = item.Value.ToObject<Template>();
                    result.importModule = valor.importModule;
                    result.moduleName = valor.moduleName;
                    result.functions = valor.functions;
                    result.importComponent = valor.importComponent;
                    result.viewchilds = valor.viewchilds;
                    result.onhidenpopup = valor.onhidenpopup;
                    result.onsetdefaulvalue = valor.onsetdefaulvalue;
                }
            }

            return result;
        }

        public string ModifyContent(string filePath, string newText, string positionToken)
        {
            string fileContent = File.ReadAllText(filePath);
            fileContent = fileContent.Replace(positionToken, newText);
            return fileContent;
        }

        public string Replace(string line, Dictionary<string, string> patterns)
        {
            string newLine = line;
            foreach (var pattern in patterns)
            {
               line = Regex.Replace(line, pattern.Key, pattern.Value);
            }

            if (line.Equals(newLine) && Regex.IsMatch(line,@"\${([^\}]*)\}"))
            {
                return string.Empty;
            }
            else
            {
                return line;
            }
        }

        public void moduleProject()
        {
            string path = @"D:\argos\ui-tms\src\app\pages\list\remesas\remesas.module.ts";
            string fileContent = File.ReadAllText(path);

            List<string> modules = new List<string>() { "Modulo1", "Modulo2", "Modulo3" };

            string modulesLine = "imports: [";
            foreach (var item in modules)
            {
                modulesLine = string.Format("{0}{1}     {2},", modulesLine, Environment.NewLine, item);
            }

            fileContent = fileContent.Replace("imports: [", modulesLine);

            string a = string.Empty;

            //Falta escribir el archivo            

            //GeneralObject nuevo = new GeneralObject();
            //nuevo.imports = new List<string>() { "CommonModule", "remesasRoutingModule", "FormsModule" };
            //nuevo.providers = new List<string>() { "CommonModule", "remesasRoutingModule", "FormsModule" };
            //nuevo.declarations = new List<string>() { "CommonModule", "remesasRoutingModule", "FormsModule" };
            //string esto = Newtonsoft.Json.JsonConvert.SerializeObject(nuevo);

            //substring = substring.Replace("\"", "\\u022");
            //object o = Newtonsoft.Json.JsonConvert.DeserializeObject(substring);


            //GeneralObject simpleMessage = Newtonsoft.Json.JsonConvert.DeserializeObject<GeneralObject>(substring);

            //var objeto = JObject.Parse(substring);

            //List<WorkDefinition> jobs = new List<WorkDefinition>();

            //foreach (var item in objeto)
            //{
            //    string key = item.Key;
            //    //WorkDefinition newWork = new WorkDefinition();
            //    //newWork.ComponentName = item.Key;
            //    //FieldsCollection Fields = item.Value.ToObject<FieldsCollection>();
            //    //newWork.ObjectsList = Fields.ListFields;
            //    //jobs.Add(newWork);

            //}
        }

        public void RoutingProject(){
            string path = @"D:\argos\ui-tms\src\app\app.routing.ts";
            string fileContent = File.ReadAllText(path);

            fileContent = fileContent.Replace("\r\n", "").Replace("\n", "").Replace("\r", "");
            string pattern = string.Empty;
            string substring = string.Empty;
            //@"\${1:([^\}]*)\}"
            pattern = @"children: \[(.*)\}\]";

            Match match = Regex.Match(fileContent, pattern);

            if (match.Success)
            {
                substring = match.Value.TrimStart(new char[] { '(' }).TrimEnd(new char[] { ')' });
            }
        }
    }
}
