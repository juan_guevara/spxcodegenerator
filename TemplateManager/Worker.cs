﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TemplateManager
{

    static class Extensions
    {
        public static void ForEach<T>(this IEnumerable<T> ie, Action<T> action)
        {
            foreach (var i in ie)
            {
                action(i);
            }
        }
    }

    public static partial class ExtensionMethods
    {
        public static JObject SetPropertyContent(this JObject source, string name, object content)
        {
            var prop = source.Property(name);

            if (prop == null)
            {
                prop = new JProperty(name, content);

                source.Add(prop);
            }
            else
            {
                prop.Value = JContainer.FromObject(content);
            }

            return source;
        }
    }

    public class Worker
    {
        private const string INDENT_STRING = "    ";

        public void ExecuteWork(string workDefinitionPath, Dictionary<string, string> paths)
        {
            string schemaPath = System.Configuration.ConfigurationManager.AppSettings.Get("schemaPath");

            TemplateValidation validator = new TemplateValidation();
            ValidateRequest validationRequest = new ValidateRequest();

            String workDefinition = File.ReadAllText(workDefinitionPath);
            String schema = File.ReadAllText(schemaPath);

            validationRequest.Json = workDefinition;
            validationRequest.Schema = schema;

            ValidateResponse validationResult = validator.Validate(validationRequest);

            if (!validationResult.Valid)
            {
                string errors = string.Join("\r\n", validationResult.Errors.Select(t => $"{t.Message} {t.Path} {t.Schema.Description} " +
                $"{ string.Join("\r\n", t.ChildErrors.Select(y => $"{y.Message} {y.Path} {y.Schema.Description} {string.Join("\r\n", y.ChildErrors.Select(z => $"{z.Message} {z.Path} {z.Schema.Description}").ToArray())}").ToArray())}").ToArray());
                throw new Exception(errors);
            }

            List<ModuleDefinition> modules = ReadWork(workDefinitionPath);
            List<string> componentsNames = new List<string>();

            foreach (var module in modules)
            {
                foreach (var component in module.ComponentList)
                {
                    componentsNames.Add(component.ComponentName);
                }
            }

            foreach (var module in modules)
            {
                List<ComponentDefinition> components = module.ComponentList.ToList<ComponentDefinition>();
                foreach (var component in components)
                {
                    ExecuteRoutines(component, module, componentsNames, paths);

                    if (component.Principal)
                    {
                        RegisterModule(component, module, componentsNames);
                    }
                }
            }

        }

        public string FormatJson(string str)
        {
            var indent = 0;
            var quoted = false;
            var sb = new StringBuilder();
            for (var i = 0; i < str.Length; i++)
            {
                var ch = str[i];
                switch (ch)
                {
                    case '{':
                    case '[':
                        sb.Append(ch);
                        if (!quoted)
                        {
                            sb.AppendLine();
                            Enumerable.Range(0, ++indent).ForEach(item => sb.Append(INDENT_STRING));
                        }
                        break;
                    case '}':
                    case ']':
                        if (!quoted)
                        {
                            sb.AppendLine();
                            Enumerable.Range(0, --indent).ForEach(item => sb.Append(INDENT_STRING));
                        }
                        sb.Append(ch);
                        break;
                    case '"':
                        sb.Append(ch);
                        bool escaped = false;
                        var index = i;
                        while (index > 0 && str[--index] == '\\')
                            escaped = !escaped;
                        if (!escaped)
                            quoted = !quoted;
                        break;
                    case ',':
                        sb.Append(ch);
                        if (!quoted)
                        {
                            sb.AppendLine();
                            Enumerable.Range(0, indent).ForEach(item => sb.Append(INDENT_STRING));
                        }
                        break;
                    case ':':
                        sb.Append(ch);
                        if (!quoted)
                            sb.Append(" ");
                        break;
                    default:
                        sb.Append(ch);
                        break;
                }
            }
            return sb.ToString();
        }

        public enum Language
        {
            Spanish,
            English
        }

        public void RegisterModuleInAppRouting(string routingPath, string routingappTemplatePath, ModuleDefinition module)
        {
            FilesBuilder fb = new FilesBuilder();

            Dictionary<string, string> patterns = new Dictionary<string, string>();

            patterns = new Dictionary<string, string>(){
                    //{@"\${1:([^\}]*)\}", module.ModuleName.ToLower()},
                    //{@"\${2:([^\}]*)\}", module.ModuleName},
                };

            string routingText = fb.BuildContent(routingappTemplatePath, "routingapp", patterns);


            //string result = string.Empty;

            string initialStringFinded = @"component: FullLayoutComponent";
            String content = File.ReadAllText(routingPath);
            //string modulename = module.ModuleName.ToString();

            //if (!content.Contains(modulename))
            //{
            //    string originalContent = content;
            //    content = content.Substring(content.IndexOf(initialStringFinded) + initialStringFinded.Length, content.Length - content.IndexOf(initialStringFinded) - initialStringFinded.Length);

            //    initialStringFinded = @"children:";
            //    content = content.Substring(content.IndexOf(initialStringFinded) + initialStringFinded.Length, content.Length - content.IndexOf(initialStringFinded) - initialStringFinded.Length);
            //    content = content.Substring(2, content.Length - 2);
            //    string fragment = content;

            //    content = string.Format("{0}\r\n{1}", routingText, content);

            //    content = originalContent.Replace(fragment, content);

            //    File.WriteAllText(routingPath, content);
            //}
            //else
            //{
            //    //throw new Exception(string.Format("El modulo {0} ya se encuentra registrado en el routing.", modulename));
            //    //Informacion el modulo ya se encuentra registrado en el routing.
            //}
        }


        public void CreateDictionary(string dictionaryPath, string dictionaryKey, ObjectDefinition[] properties, Language language)
        {
            try
            {
                dictionaryKey = string.Format("lst{0}", dictionaryKey.ToLower());
                String content = File.ReadAllText(dictionaryPath);
                JObject objeto = JObject.Parse(content);
                string key = string.Empty;
                string value = string.Empty;
                string lines = string.Empty;

                foreach (var property in properties)
                {
                    key = property.Name;
                    switch (language)
                    {
                        case Language.Spanish:
                            value = property.Spanish;
                            break;
                        case Language.English:
                            value = property.English;
                            break;
                        default:
                            break;
                    }

                    lines = string.Format(@"{3} {2}{0}{2} : {2}{1}{2} {4}", key.ToUpper(), value, (char)34, lines, (char)44);
                }

                string valornuevo = string.Format("{0} {1} {2}", (char)123, lines, (char)125);
                if (objeto.Property(dictionaryKey) == null)
                    objeto.Add(dictionaryKey, "");

                objeto.Property(dictionaryKey).Value = JToken.Parse(valornuevo);
                string serialized = Newtonsoft.Json.JsonConvert.SerializeObject(objeto);

                File.WriteAllText(dictionaryPath, FormatJson(serialized));
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<ModuleDefinition> ReadWork(string templatePath)
        {
            string result = string.Empty;
            String content = File.ReadAllText(templatePath);
            var objeto = JObject.Parse(content);

            List<ModuleDefinition> jobs = new List<ModuleDefinition>();

            foreach (var item in objeto)
            {
                ModuleDefinition newModule = new ModuleDefinition();
                //newModule.ModuleName = item.Key;
                ComponentCollection components = item.Value.ToObject<ComponentCollection>();
                newModule.ComponentList = components.ComponentsList;
                jobs.Add(newModule);
            }

            return jobs;
        }

        private static void CreateDirectory(string path)
        {
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
        }

        private string CreateModelLines(ObjectDefinition[] modelProperties, string objectType)
        {
            string properties = string.Empty;

            foreach (var item in modelProperties)
            {
                string modelType = string.Empty;
                if (item.ObjectType.Equals(objectType))
                {
                    modelType = homologateTypes(item.Type);
                    properties = string.Format("{0} {1}        {2}:{3};", properties, Environment.NewLine, item.Name, modelType);
                }

            }

            return properties;
        }

        private string homologateTypes(string itemType)
        {
            string homologation = string.Empty;

            if (itemType.ToLower().Contains("date"))
            {
                homologation = "Date";
            }
            else
            {
                if (itemType.ToLower().Contains("char") || itemType.ToLower().Contains("string"))
                {
                    homologation = "string";
                }
                else
                {
                    if (itemType.ToLower().Contains("boolean"))
                    {
                        homologation = "boolean";
                    }
                    else
                    {
                        homologation = "Number";
                    }
                }
            }

            return homologation;
        }

        private string CreateColumnLines(ObjectDefinition[] modelProperties, string componentName, string objectType)
        {
            try
            {
                string columnsLines = string.Empty;

                foreach (var item in modelProperties)
                {
                    string cellTemplate = string.Empty;
                    string width = string.Empty;
                    string dateFormatt = string.Empty;

                    if (item.ObjectType.Equals(objectType))
                    {
                        if (!string.IsNullOrEmpty(item.CellTemplate))
                        {
                            switch (item.CellTemplate.ToLower())
                            {
                                case "celltemplatecode":
                                    cellTemplate = "cellTemplate=\"cellTemplateCode\" ";
                                    break;
                                default:
                                    cellTemplate = string.Empty;
                                    break;
                            }
                        }

                        if (item.Width > 0)
                        {
                            width = string.Format(" width=\"{0}\" ", item.Width);
                        }

                        if (item.Type.ToLower().Contains("date"))
                        {
                            dateFormatt = " dataType=\"datetime\" format=\"dd/MM/yyyy HH:mm\" ";
                        }

                        columnsLines = string.Format("{0} {1}     <dxi-column [showInColumnChooser]=\"true\" {3}dataField=\"{4}\" {8}{9}caption=\"{6}{6}'lst{5}.{2}' | translate {7}{7}\"></dxi-column>", columnsLines, Environment.NewLine, item.Name.ToUpper(), width, item.Name, componentName.ToLower(), (char)123, (char)125, cellTemplate, dateFormatt);
                    }
                }

                return columnsLines;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private int GetFormColumnsCount(ObjectDefinition[] fields, int maxColumnsByForm)
        {
            string FormField = string.Empty;

            int groupsCount = 0;
            string currentColumn = "";
            foreach (var item in fields)
            {
                if (item.ObjectType.Equals("FormField") && !string.IsNullOrEmpty(item.Group) && !item.Group.Equals(currentColumn, StringComparison.InvariantCultureIgnoreCase))
                {
                    currentColumn = item.Group;
                    groupsCount = groupsCount + 1;
                }
            }

            if (groupsCount > maxColumnsByForm)
                groupsCount = maxColumnsByForm;

            return groupsCount;
        }

        public string CreateGroup(ObjectDefinition[] fields, string templatePath, string componentName)
        {
            FilesBuilder fb = new FilesBuilder();
            string result = string.Empty;

            string currentColumn = "";

            foreach (var item in fields)
            {
                if (item.ObjectType.Equals("FormField") && !string.IsNullOrEmpty(item.Group) && !item.Group.Equals(currentColumn, StringComparison.InvariantCultureIgnoreCase))
                {
                    Dictionary<string, string> patterns = new Dictionary<string, string>(){
                    {@"\${1:([^\}]*)\}", item.Group},
                    {@"\${2:([^\}]*)\}", CreateGroupItems(fields, templatePath, componentName, item.Group)}};
                    result = string.Format("{0}{1}{2}", result, Environment.NewLine, fb.BuildContent(templatePath, "group", patterns));
                    currentColumn = item.Group;
                }
            }

            return result;
        }


        public string CreateChildComponentVisualization(ModuleDefinition module, string templatePath, string componentName)
        {
            string containerType = "Tab";
            string result = string.Empty;

            FilesBuilder fb = new FilesBuilder();

            foreach (var component in module.ComponentList)
            {
                if (!string.IsNullOrEmpty(component.Parent) && component.Parent.Equals(componentName, StringComparison.InvariantCultureIgnoreCase))
                {
                    Dictionary<string, string> patterns = new Dictionary<string, string>(){
                    {@"\${1:([^\}]*)\}", component.ComponentName.ToLower()},
                    {@"\${2:([^\}]*)\}", component.ComponentName.ToLower()}
                    };
                    result = string.Format("{0}{1}{2}", Environment.NewLine, result, fb.BuildContent(templatePath, containerType, patterns));
                }
            }

            return result;
        }

        public string CreateImportComponents(ModuleDefinition module, string templatePath)
        {
            string result = string.Empty;

            FilesBuilder fb = new FilesBuilder();

            foreach (var component in module.ComponentList)
            {
                Dictionary<string, string> patterns = new Dictionary<string, string>(){
                    {@"\${1:([^\}]*)\}", component.ComponentName},
                    {@"\${2:([^\}]*)\}", component.ComponentName.ToLower()}
                    };
                result = string.Format("{0}{1}", fb.BuildContent(templatePath, "importModuleComponent", patterns), result);
            }

            return result.TrimStart(new char[] { '\r' }).TrimStart(new char[] { '\n' });
        }

        public string ImportChildsComponents(ModuleDefinition module, string templatePath, string componentName)
        {
            string result = string.Empty;

            FilesBuilder fb = new FilesBuilder();

            foreach (var component in module.ComponentList)
            {
                if (!string.IsNullOrEmpty(component.Parent) && component.Parent.Equals(componentName, StringComparison.InvariantCultureIgnoreCase))
                {
                    Dictionary<string, string> patterns = new Dictionary<string, string>(){
                    {@"\${1:([^\}]*)\}", component.ComponentName},
                    {@"\${2:([^\}]*)\}", component.ComponentName.ToLower()},
                    };
                    result = string.Format("{0}{1}", fb.BuildContent(templatePath, "importChildComponents", patterns), result);
                }
            }

            return result.TrimStart(new char[] { '\r' }).TrimStart(new char[] { '\n' });
        }

        public string CreateSetPreLoadToEditionDataFromParent(ModuleDefinition module, string templatePath, string componentName)
        {
            string result = string.Empty;

            FilesBuilder fb = new FilesBuilder();

            foreach (var component in module.ComponentList)
            {
                if (component.ComponentName.Equals(componentName, StringComparison.InvariantCultureIgnoreCase))
                {
                    foreach (var myObject in component.ObjectsList)
                    {
                        if (myObject.Preload != null && myObject.Preload.Exists(x => x.Name.Equals("source") && x.Value.Equals("parent")))
                        {
                            Dictionary<string, string> patterns = new Dictionary<string, string>(){
                            {@"\${1:([^\}]*)\}", myObject.Name}
                            };
                            result = string.Format("{0}{1}", fb.BuildContent(templatePath, "setPreLoadToEditionDataFromParent", patterns), result);
                        }
                    }
                }
            }

            return result.TrimStart(new char[] { '\r' }).TrimStart(new char[] { '\n' });
        }


        public string CreateSetPreLoadFieldFromParent(ModuleDefinition module, string templatePath, string componentName)
        {
            string result = string.Empty;

            FilesBuilder fb = new FilesBuilder();

            foreach (var component in module.ComponentList)
            {
                if (!string.IsNullOrEmpty(component.Parent) && component.Parent.Equals(componentName, StringComparison.InvariantCultureIgnoreCase))
                {
                    foreach (var myObject in component.ObjectsList)
                    {
                        if (myObject.Preload != null && myObject.Preload.Exists(x => x.Name.Equals("source") && x.Value.Equals("parent")))
                        {
                            Dictionary<string, string> patterns = new Dictionary<string, string>(){
                            {@"\${1:([^\}]*)\}", component.ComponentName},
                            {@"\${2:([^\}]*)\}", myObject.Name},
                            {@"\${3:([^\}]*)\}", myObject.Preload.Find(x => x.Name.Equals("Field")).Value},
                            };
                            result = string.Format("{0}{1}", fb.BuildContent(templatePath, "setPreLoadFieldFromParent", patterns), result);
                        }
                    }
                }
            }

            return result.TrimStart(new char[] { '\r' }).TrimStart(new char[] { '\n' });
        }

        public string CreatePreLoadVariablesFromParent(ModuleDefinition module, string templatePath, string componentName)
        {
            string result = string.Empty;

            FilesBuilder fb = new FilesBuilder();

            foreach (var component in module.ComponentList)
            {
                if (component.ComponentName.Equals(componentName, StringComparison.InvariantCultureIgnoreCase))
                {
                    foreach (var myObject in component.ObjectsList)
                    {
                        if (myObject.Preload != null && myObject.Preload.Exists(x => x.Name.Equals("source") && x.Value.Equals("parent")))
                        {
                            Dictionary<string, string> patterns = new Dictionary<string, string>(){
                            {@"\${1:([^\}]*)\}", myObject.Name}
                            ,{@"\${2:([^\}]*)\}", homologateTypes(myObject.Type)},
                            };
                            result = string.Format("{0}{1}", fb.BuildContent(templatePath, "preLoadVariablesFromParent", patterns), result);
                        }
                    }
                }
            }

            return result.TrimStart(new char[] { '\r' }).TrimStart(new char[] { '\n' });
        }



        public string CreateViewChildsComponents(ModuleDefinition module, string templatePath, string componentName)
        {
            string result = string.Empty;

            FilesBuilder fb = new FilesBuilder();

            foreach (var component in module.ComponentList)
            {
                //if (!component.Principal)
                if (!string.IsNullOrEmpty(component.Parent) && component.Parent.Equals(componentName, StringComparison.InvariantCultureIgnoreCase))
                {
                    Dictionary<string, string> patterns = new Dictionary<string, string>(){
                    {@"\${1:([^\}]*)\}", component.ComponentName},
                    };
                    result = string.Format("{0}{1}", fb.BuildContent(templatePath, "viewChildComponents", patterns), result);
                }
            }

            return result.TrimStart(new char[] { '\r' }).TrimStart(new char[] { '\n' });
        }

        public string CreateDefaultValueChildsComponents(ModuleDefinition module, string templatePath, string componentName)
        {
            string result = string.Empty;

            FilesBuilder fb = new FilesBuilder();

            foreach (var component in module.ComponentList)
            {
                //if (!component.Principal)
                if (!string.IsNullOrEmpty(component.Parent) && component.Parent.Equals(componentName, StringComparison.InvariantCultureIgnoreCase))
                {
                    Dictionary<string, string> patterns = new Dictionary<string, string>(){
                    {@"\${1:([^\}]*)\}", component.ComponentName},
                    {@"\${2:([^\}]*)\}", component.Get},
                    {@"\${code([^\}]*)\}", "${" + string.Format(@"this.editionData.{0}",component.Binding) + "}"}
                    };
                    result = string.Format("{0}{1}", fb.BuildContent(templatePath, "defaultValueChildComponents", patterns), result);
                }
            }

            return result.TrimStart(new char[] { '\r' }).TrimStart(new char[] { '\n' });
        }

        public string GetComponentNames(ModuleDefinition module)
        {
            string result = string.Empty;
            foreach (var component in module.ComponentList)
            {
                result = string.Format("{0}{1},{2}", Environment.NewLine, string.Format("Lst{0}Component", component.ComponentName), result);
            }

            return result.TrimEnd(new char[] { ',' });
        }

        public ModuleReferenceDefinition CreateComponentReferences(ObjectDefinition[] fields, string templatePath, string jobName)
        {
            FilesBuilder fb = new FilesBuilder();
            List<ModuleReferenceDefinition> componentReferences = new List<ModuleReferenceDefinition>();
            ModuleReferenceDefinition result = new ModuleReferenceDefinition() { importModule = string.Empty, moduleName = string.Empty };
            result.functionsText = string.Empty;
            result.importComponent = string.Empty;
            result.viewchilds = string.Empty;
            result.onhidenpopup = string.Empty;
            result.onsetdefaulvalue = string.Empty;

            string currentTemplate = "";

            foreach (var item in fields)
            {
                Dictionary<string, string> patterns = new Dictionary<string, string>();
                patterns.Add(@"\${1:([^\}]*)\}", item.Name);

                if (item.ObjectType.Equals("FormField") && !string.IsNullOrEmpty(item.CellTemplate) && !item.CellTemplate.Equals(currentTemplate, StringComparison.InvariantCultureIgnoreCase))
                {
                    ModuleReferenceDefinition definition = fb.GetComponentReferences(templatePath, item.CellTemplate);
                    if (!string.IsNullOrEmpty(definition.importModule)
                        && !result.importModule.Contains(definition.importModule))
                    {
                        result.importModule = string.Format("{0}{1}{2}", result.importModule, definition.importModule, Environment.NewLine);
                    }

                    if (!string.IsNullOrEmpty(definition.moduleName)
                        && !result.moduleName.Contains(definition.moduleName))
                    {
                        result.moduleName = string.Format("{0}{1},{2}", result.moduleName, definition.moduleName, Environment.NewLine);
                    }

                    if (!string.IsNullOrEmpty(definition.importComponent)
                        && !result.importComponent.Contains(definition.importComponent))
                    {
                        result.importComponent = string.Format("{0}{1}{2}", result.importComponent, fb.Replace(definition.importComponent, patterns), Environment.NewLine);
                    }

                    if (!string.IsNullOrEmpty(definition.viewchilds)
                        && !result.viewchilds.Contains(definition.viewchilds))
                    {
                        result.viewchilds = string.Format("{0}{1}{2}", result.viewchilds, fb.Replace(definition.viewchilds, patterns), Environment.NewLine);
                    }

                    if (!string.IsNullOrEmpty(definition.onhidenpopup)
                        && !result.onhidenpopup.Contains(definition.onhidenpopup))
                    {
                        result.onhidenpopup = string.Format("{0}{1}{2}", result.onhidenpopup, fb.Replace(definition.onhidenpopup, patterns), Environment.NewLine);
                    }

                    if (!string.IsNullOrEmpty(definition.onsetdefaulvalue)
                        && !result.onsetdefaulvalue.Contains(definition.onsetdefaulvalue))
                    {
                        result.onsetdefaulvalue = string.Format("{0}{1}{2}", result.onsetdefaulvalue, fb.Replace(definition.onsetdefaulvalue, patterns), Environment.NewLine);
                    }

                    foreach (var functionLine in definition.functions)
                    {
                        result.functionsText = string.Format("{0}{1}{2}", result.functionsText, fb.Replace(functionLine, patterns), Environment.NewLine);
                    }
                }
            }

            return result;
        }

        public string CreateGroupItems(ObjectDefinition[] fields, string templatePath, string componentName, string group)
        {
            string fieldsInGroup = string.Empty;

            foreach (var item in fields)
            {
                if (item.ObjectType.Equals("FormField") && !string.IsNullOrEmpty(item.Group) && item.Group.Equals(group, StringComparison.InvariantCultureIgnoreCase))
                {
                    string visible = string.Empty;
                    string disable = string.Empty;
                    string width = string.Empty;
                    string widthNumber = string.Empty;

                    if (!string.IsNullOrEmpty(item.BehaviorInEdition))
                    {
                        switch (item.BehaviorInEdition.ToLower())
                        {
                            case "disable":
                                disable = " disabled: disableInputs";
                                break;

                            case "hide":
                                visible = "[visible]=\"visibleInputs\"";
                                break;
                        }
                    }
                    else
                    {
                        disable = " disabled: false";
                    }

                    if (item.Width > 0)
                    {
                        width = string.Format(" width:'{0}', ", item.Width.ToString());
                        widthNumber = string.Format("[width]=\"${0}\"", item.Width.ToString());
                    }

                    FilesBuilder fb = new FilesBuilder();
                    Dictionary<string, string> patterns = new Dictionary<string, string>(){
                    {@"\${1:([^\}]*)\}", item.Name},
                    {@"\${2:([^\}]*)\}", string.Format("lst{0}.{1}", componentName.ToLower(), item.Name.ToUpper())},
                    {@"\${3:([^\}]*)\}", visible},
                    {@"\${4:([^\}]*)\}", disable},
                    {@"\${5:([^\}]*)\}", width},
                    {@"\${6:([^\}]*)\}", widthNumber}
                    };

                    string template = item.CellTemplate;
                    //string template = string.Empty;
                    if (string.IsNullOrEmpty(template))
                    {
                        string itemType = homologateTypes(item.Type);
                        switch (itemType.ToLower())
                        {
                            case "date":
                                template = "Html_dateboxedicion";
                                break;
                            case "number":
                                template = "Html_numberboxedicion";
                                break;
                            default:
                                template = "Html_textboxedicion";
                                break;
                        }
                    }

                    if (item.TemplateParameters != null)
                    {
                        foreach (var parameter in item.TemplateParameters)
                        {
                            string patternText = @"\${" + parameter.Name + @"([^\}]*)\}";
                            string Value = parameter.Value;

                            patterns.Add(patternText, Value);
                        }
                    }


                    string field = fb.BuildContent(templatePath, template, patterns);


                    fieldsInGroup = string.Format("{0}{1}{2}", fieldsInGroup, Environment.NewLine, field);
                }
            }

            return fieldsInGroup;
        }

        private void RegisterModule(ComponentDefinition job, ModuleDefinition module, List<string> componentsNames)
        {
            string ComponentName = job.ComponentName;

            string templatesPath = System.Configuration.ConfigurationManager.AppSettings.Get("templatesPath");

            string routingappTemplatePath = Path.Combine(templatesPath, "typescript.json");
            string componentTemplatePath = Path.Combine(templatesPath, "typescript.json");
            string componentSpecTemplatePath = Path.Combine(templatesPath, "typescript.json");
            string modelTemplatePath = Path.Combine(templatesPath, "typescript.json");
            string htmlTemplatePath = Path.Combine(templatesPath, "html.json");
            string serviceTemplatePath = Path.Combine(templatesPath, "typescript.json");
            string routingTemplatePath = Path.Combine(templatesPath, "typescript.json");
            string moduleTemplatePath = Path.Combine(templatesPath, "typescript.json");
            string moduleSpecTemplatePath = Path.Combine(templatesPath, "typescript.json");

            string dictionaryPath = System.Configuration.ConfigurationManager.AppSettings.Get("dictionaryPath");

            //string modulePath = Path.Combine(System.Configuration.ConfigurationManager.AppSettings.Get("modulePath"), module.ModuleName.ToLower());
            string servicePath = Path.Combine(System.Configuration.ConfigurationManager.AppSettings.Get("servicePath"), ComponentName.ToLower());
            string modelPath = System.Configuration.ConfigurationManager.AppSettings.Get("modelPath");

            //string componentPath = Path.Combine(modulePath, string.Format("lst{0}", ComponentName.ToLower()));
            string routingPath = System.Configuration.ConfigurationManager.AppSettings.Get("routingPath");

            FilesBuilder fb = new FilesBuilder();

            List<string> paths = new List<string>(){
                    //modulePath,
                };

            foreach (var path in paths)
            {
                CreateDirectory(path);
            }

            Dictionary<string, string> patterns = new Dictionary<string, string>();
            //routingapp
            RegisterModuleInAppRouting(routingPath, routingappTemplatePath, module);

            //Modulo
            //CreateModule(job, htmlTemplatePath, module, ComponentName, moduleTemplatePath, modulePath);


            ////Modulo_spec
            //patterns = new Dictionary<string, string>(){
            //        {@"\${1:([^\}]*)\}", ComponentName},
            //        {@"\${2:([^\}]*)\}", ComponentName}
            //    };
            //File.WriteAllText(Path.Combine(modulePath, string.Format(@"{0}.module.spec.ts", module.ModuleName.ToLower())), fb.BuildContent(moduleSpecTemplatePath, "module_spec", patterns));

            ////Routing
            //patterns = new Dictionary<string, string>(){
            //        {@"\${1:([^\}]*)\}", GetModuleRoutesReferences(routingTemplatePath, componentsNames)},
            //        {@"\${2:([^\}]*)\}", module.ModuleName},
            //        {@"\${3:([^\}]*)\}", GetModuleRoutes(routingTemplatePath, module)}
            //    };
            //File.WriteAllText(Path.Combine(modulePath, string.Format(@"{0}.routing.module.ts", module.ModuleName.ToLower())), fb.BuildContent(routingTemplatePath, "routing", patterns));

        }

        private void CreateModule(ComponentDefinition job, string htmlTemplatePath, ModuleDefinition module, string ComponentName, string moduleTemplatePath, string modulePath)
        {
            //FilesBuilder fb = new FilesBuilder();

            //Dictionary<string, string> patterns = new Dictionary<string, string>();

            //ModuleReferenceDefinition fieldTemplateReferences = CreateComponentReferences(job.ObjectsList, htmlTemplatePath, job.ComponentName);

            //patterns = new Dictionary<string, string>(){
            //         {@"\${1:([^\}]*)\}", string.Format("{0}RoutingModule",module.ModuleName)}
            //        ,{@"\${2:([^\}]*)\}", ComponentName}
            //        ,{@"\${3:([^\}]*)\}", GetComponentNames(module)}
            //        ,{@"\${4:([^\}]*)\}", string.Format("{0}Module",module.ModuleName)}
            //        ,{@"\${5:([^\}]*)\}", ComponentName.ToLower()}
            //        ,{@"\${6:([^\}]*)\}", fieldTemplateReferences.importModule}
            //        ,{@"\${7:([^\}]*)\}", fieldTemplateReferences.moduleName}
            //        ,{@"\${8:([^\}]*)\}", module.ModuleName.ToLower()}
            //        ,{@"\${9:([^\}]*)\}", CreateImportComponents(module, moduleTemplatePath)}
            //    };
            //File.WriteAllText(Path.Combine(modulePath, string.Format(@"{0}.module.ts", module.ModuleName.ToLower())), fb.BuildContent(moduleTemplatePath, "modulo", patterns));
        }

        public void ExecuteRoutines(ComponentDefinition job, ModuleDefinition module, List<string> componentNames, Dictionary<string, string> workerRoutes)
        {
            Console.WriteLine(workerRoutes);

            string ComponentName = job.ComponentName;

            string templatesPath = System.Configuration.ConfigurationManager.AppSettings.Get("templatesPath");
            string routingappTemplatePath = Path.Combine(templatesPath, "typescript.json");
            string componentTemplatePath = Path.Combine(templatesPath, "typescript.json");
            string componentSpecTemplatePath = Path.Combine(templatesPath, "typescript.json");
            string modelTemplatePath = Path.Combine(templatesPath, "typescript.json");
            string htmlTemplatePath = Path.Combine(templatesPath, "html.json");
            string serviceTemplatePath = Path.Combine(templatesPath, "typescript.json");
            string routingTemplatePath = Path.Combine(templatesPath, "typescript.json");
            string moduleTemplatePath = Path.Combine(templatesPath, "typescript.json");
            string moduleSpecTemplatePath = Path.Combine(templatesPath, "typescript.json");

            string dictionaryPath = workerRoutes["dictionaryPath"];
            string schemaPath = workerRoutes["schemaPath"];
            string modulePath = workerRoutes["modulePath"];
            string servicePath = Path.Combine(workerRoutes["servicePath"], ComponentName.ToLower());
            string modelPath = workerRoutes["modelPath"];
            string componentPath = Path.Combine(modulePath, string.Format("lst{0}", ComponentName.ToLower()));
            string routingPath = workerRoutes["routingPath"];

            FilesBuilder fb = new FilesBuilder();


            //fb.RoutingProject();

            List<string> paths = new List<string>(){
                    modulePath,
                    modelPath,
                    servicePath,
                    componentPath
                };

            foreach (var path in paths)
            {
                CreateDirectory(path);
            }

            Dictionary<string, string> patterns = new Dictionary<string, string>();

            //string form = CreateForm(job);

            CreateDictionary(Path.Combine(dictionaryPath, "es.json"), job.ComponentName, job.ObjectsList, Language.Spanish);
            CreateDictionary(Path.Combine(dictionaryPath, "en.json"), job.ComponentName, job.ObjectsList, Language.English);


            //scss
            File.WriteAllText(Path.Combine(componentPath, string.Format(@"lst{0}.component.scss", ComponentName.ToLower())), fb.BuildContent(componentTemplatePath, "scss", patterns));

            ModuleReferenceDefinition fieldTemplateReferences = CreateComponentReferences(job.ObjectsList, htmlTemplatePath, job.ComponentName);

            //html
            if (job.ObjectsList.Count(x => x.ObjectType.Equals("ListColumn", StringComparison.InvariantCultureIgnoreCase)) > 0)
            {
                patterns = HtmlPatternsGenerator.BuildPatterns(job, htmlTemplatePath, module);
                File.WriteAllText(Path.Combine(componentPath, string.Format(@"lst{0}.component.html", ComponentName.ToLower())), fb.BuildContent(htmlTemplatePath, "Html_list", patterns));
            }

            if (!job.Principal)
            {
                //Componente
                patterns = new Dictionary<string, string>(){
                     {@"\${1:([^\}]*)\}", ComponentName.ToLower()}
                    ,{@"\${2:([^\}]*)\}", string.Format("Lst{0}",ComponentName)}
                    ,{@"\${3:([^\}]*)\}", string.Format("srv{0}",ComponentName)}
                    ,{@"\${4:([^\}]*)\}", string.Format("{0}Service",ComponentName)}
                    ,{@"\${5:([^\}]*)\}", ComponentName}
                    ,{@"\${6:([^\}]*)\}", ".EightRow2Col_width"}
                    ,{@"\${7:([^\}]*)\}", ".EightRow2Col_heigh"}
                    ,{@"\${8:([^\}]*)\}", string.Format("{0}Model",ComponentName)}
                    ,{@"\${9:([^\}]*)\}", GetDetailCriteria(job.ObjectsList)}
                    ,{@"\${10:([^\}]*)\}", fieldTemplateReferences.functionsText}
                    ,{@"\${11:([^\}]*)\}", fieldTemplateReferences.importComponent}
                    ,{@"\${12:([^\}]*)\}", fieldTemplateReferences.viewchilds}
                    ,{@"\${13:([^\}]*)\}", fieldTemplateReferences.onhidenpopup}
                    ,{@"\${14:([^\}]*)\}", fieldTemplateReferences.onsetdefaulvalue}
                    ,{@"\${15:([^\}]*)\}", job.Get}
                    ,{@"\${16:([^\}]*)\}", CreateDefaultValueChildsComponents(module, componentTemplatePath, ComponentName)}
                    ,{@"\${17:([^\}]*)\}", CreateViewChildsComponents(module, componentTemplatePath, ComponentName)}
                    ,{@"\${18:([^\}]*)\}", ImportChildsComponents(module, componentTemplatePath, ComponentName)}
                    ,{@"\${19:([^\}]*)\}", CreateSetPreLoadFieldFromParent(module, componentTemplatePath, ComponentName)}
                    ,{@"\${20:([^\}]*)\}", CreatePreLoadVariablesFromParent(module, componentTemplatePath, ComponentName)}
                    ,{@"\${21:([^\}]*)\}", CreateSetPreLoadToEditionDataFromParent(module, componentTemplatePath, ComponentName)}
                    ,{@"\${22:([^\}]*)\}", CreateContextMenuItemsCollection(componentTemplatePath, ComponentName, module)}
                    ,{@"\${23:([^\}]*)\}", CreateMenuItemsVariable(componentTemplatePath, ComponentName, module)}
                };
                File.WriteAllText(Path.Combine(componentPath, string.Format(@"lst{0}.component.ts", ComponentName.ToLower())), fb.BuildContent(componentTemplatePath, "componente", patterns));
            }

            if (job.Principal)
            {
                //Componente
                patterns = new Dictionary<string, string>(){
                     {@"\${1:([^\}]*)\}", ComponentName.ToLower()}
                    ,{@"\${2:([^\}]*)\}", string.Format("Lst{0}",ComponentName)}
                    ,{@"\${3:([^\}]*)\}", string.Format("srv{0}",ComponentName)}
                    ,{@"\${4:([^\}]*)\}", string.Format("{0}Service",ComponentName)}
                    ,{@"\${5:([^\}]*)\}", ComponentName}
                    ,{@"\${6:([^\}]*)\}", ".EightRow2Col_width"}
                    ,{@"\${7:([^\}]*)\}", ".EightRow2Col_heigh"}
                    ,{@"\${8:([^\}]*)\}", string.Format("{0}Model",ComponentName)}
                    ,{@"\${9:([^\}]*)\}", GetDetailCriteria(job.ObjectsList)}
                    ,{@"\${10:([^\}]*)\}", fieldTemplateReferences.functionsText}
                    ,{@"\${11:([^\}]*)\}", fieldTemplateReferences.importComponent}
                    ,{@"\${12:([^\}]*)\}", fieldTemplateReferences.viewchilds}
                    ,{@"\${13:([^\}]*)\}", fieldTemplateReferences.onhidenpopup}
                    ,{@"\${14:([^\}]*)\}", fieldTemplateReferences.onsetdefaulvalue}
                    ,{@"\${15:([^\}]*)\}", job.Get}
                    ,{@"\${16:([^\}]*)\}", CreateDefaultValueChildsComponents(module, componentTemplatePath, ComponentName)}
                    ,{@"\${17:([^\}]*)\}", CreateViewChildsComponents(module, componentTemplatePath, ComponentName)}
                    ,{@"\${18:([^\}]*)\}", ImportChildsComponents(module, componentTemplatePath, ComponentName)}
                    ,{@"\${19:([^\}]*)\}", CreateSetPreLoadFieldFromParent(module, componentTemplatePath, ComponentName)}
                    ,{@"\${20:([^\}]*)\}", CreatePreLoadVariablesFromParent(module, componentTemplatePath, ComponentName)}
                    ,{@"\${21:([^\}]*)\}", CreateSetPreLoadToEditionDataFromParent(module, componentTemplatePath, ComponentName)}
                    ,{@"\${22:([^\}]*)\}", CreateContextMenuItemsCollection(componentTemplatePath, ComponentName, module)}
                    ,{@"\${23:([^\}]*)\}", CreateMenuItemsVariable(componentTemplatePath, ComponentName, module)}
                };
                File.WriteAllText(Path.Combine(componentPath, string.Format(@"lst{0}.component.ts", ComponentName.ToLower())), fb.BuildContent(componentTemplatePath, "componente", patterns));
            }

            //Component_spec
            patterns = new Dictionary<string, string>(){
                    {@"\${1:([^\}]*)\}", ComponentName},
                    {@"\${2:([^\}]*)\}", ComponentName.ToLower()}
                };
            File.WriteAllText(Path.Combine(componentPath, string.Format(@"lst{0}.component.spec.ts", ComponentName.ToLower())), fb.BuildContent(componentSpecTemplatePath, "component_spec", patterns));

            //Modelo
            patterns = new Dictionary<string, string>(){
                    {@"\${1:([^\}]*)\}", ComponentName},
                    {@"\${2:([^\}]*)\}", CreateModelLines(job.ObjectsList, "FormField")},
                };
            File.WriteAllText(Path.Combine(modelPath, string.Format(@"{0}.model.ts", ComponentName.ToLower())), fb.BuildContent(modelTemplatePath, "modelo", patterns));

            //Servicio
            patterns = new Dictionary<string, string>(){
                    {@"\${1:([^\}]*)\}", ComponentName}
                   ,{@"\${2:([^\}]*)\}", job.SearchFunction}
                   ,{@"\${3:([^\}]*)\}", job.Post}
                   ,{@"\${4:([^\}]*)\}", job.Put}
                   ,{@"\${5:([^\}]*)\}", job.Delete}
                };
            File.WriteAllText(Path.Combine(servicePath, string.Format("{0}.service.ts", ComponentName.ToLower())), fb.BuildContent(serviceTemplatePath, "servicio", patterns));

        }

        private string CreateMenuItemsVariable(string componentTemplatePath, string componentName, ModuleDefinition module)
        {
            FilesBuilder fb = new FilesBuilder();
            string contextMenu = string.Empty;

            ComponentDefinition component = module.ComponentList.First<ComponentDefinition>(x => x.ComponentName.Equals(componentName, StringComparison.InvariantCultureIgnoreCase));

            int count = component.ObjectsList.ToList<ObjectDefinition>().FindAll(x => x.ObjectType.Equals("MenuItem", StringComparison.InvariantCultureIgnoreCase)).Count;

            if (count > 0)
            {
                contextMenu = "contextItems: any[]";
            }

            return contextMenu;
        }

        private string CreateContextMenuItemsCollection(string componentTemplatePath, string componentName, ModuleDefinition module)
        {
            FilesBuilder fb = new FilesBuilder();
            string contextMenu = string.Empty;

            ComponentDefinition component = module.ComponentList.First<ComponentDefinition>(x => x.ComponentName.Equals(componentName, StringComparison.InvariantCultureIgnoreCase));

            int count = component.ObjectsList.ToList<ObjectDefinition>().FindAll(x => x.ObjectType.Equals("MenuItem", StringComparison.InvariantCultureIgnoreCase)).Count;

            int index = 0;
            if (count > 0)
            {
                List<ObjectDefinition> menuItems = component.ObjectsList.ToList<ObjectDefinition>().FindAll(x => x.ObjectType.Equals("MenuItem", StringComparison.InvariantCultureIgnoreCase));

                StringBuilder sbItems = new StringBuilder();
                foreach (ObjectDefinition menuItem in menuItems)
                {
                    index++;
                    Dictionary<string, string> patterns = new Dictionary<string, string>(){
                        {@"\${1:([^\}]*)\}", menuItem.Spanish},
                        {@"\${2:([^\}]*)\}", index.ToString()}
                    };

                    string newContextMenu = fb.BuildContent(componentTemplatePath, "menuItem", patterns);
                    sbItems.AppendLine(newContextMenu);
                }

                StringBuilder sb = new StringBuilder();
                sb.AppendLine("this.contextItems = [");
                sb.AppendLine(sbItems.ToString());
                sb.AppendLine("];");

                contextMenu = sb.ToString();
            }

            return contextMenu;
        }

        private string CreateContextMenu(string htmlTemplatePath, string componentName, ModuleDefinition module)
        {
            FilesBuilder fb = new FilesBuilder();
            string contextMenu = string.Empty;

            ComponentDefinition component = module.ComponentList.First<ComponentDefinition>(x => x.ComponentName.Equals(componentName, StringComparison.InvariantCultureIgnoreCase));

            int count = component.ObjectsList.ToList<ObjectDefinition>().FindAll(x => x.ObjectType.Equals("MenuItem", StringComparison.InvariantCultureIgnoreCase)).Count;

            if (count > 0)
            {
                string newContextMenu = fb.BuildContent(htmlTemplatePath, "contextMenu", new Dictionary<string, string>());
                contextMenu = string.Format("{0}\n{1}", contextMenu, newContextMenu);
            }

            return contextMenu;
        }


        private string GetDetailCriteria(ObjectDefinition[] fieldDefinition)
        {
            string result = string.Empty;
            foreach (var item in fieldDefinition)
            {
                if (item.DetailParameter)
                {
                    result = item.Name;
                }
            }
            return result;
        }

        private string GetModuleRoutes(string routingTemplatePath, ModuleDefinition module)
        {
            FilesBuilder fb = new FilesBuilder();

            string routes = string.Empty;
            ComponentDefinition[] components = module.ComponentList;


            foreach (ComponentDefinition component in components)
            {

                string path = string.Empty;
                if (!component.Principal)
                {
                    path = component.ComponentName.ToLower();
                }

                string newRoute = string.Empty;
                Dictionary<string, string> patterns = new Dictionary<string, string>(){
                    {@"\${1:([^\}]*)\}", component.ComponentName},
                    {@"\${2:([^\}]*)\}", path}
                };

                newRoute = fb.BuildContent(routingTemplatePath, "routingModuleRoutes", patterns);
                routes = string.Format("{0}\n{1}", routes, newRoute);
            }

            return routes;
        }

        private string GetModuleRoutesReferences(string routingTemplatePath, List<string> components)
        {
            FilesBuilder fb = new FilesBuilder();
            string routeReferences = string.Empty;
            foreach (string component in components)
            {
                Dictionary<string, string> patterns = new Dictionary<string, string>(){
                    {@"\${1:([^\}]*)\}", component},
                    {@"\${2:([^\}]*)\}", component.ToLower()}
                };

                string newRouteReference = fb.BuildContent(routingTemplatePath, "routingModuleReferences", patterns);
                routeReferences = string.Format("{0}\n{1}", routeReferences, newRouteReference);
            }

            return routeReferences;
        }

    }

    public class ConfigManager
    {

        public Dictionary<string, string> _WorkRoutes;

        private string configResultPath;

        public ConfigManager(Dictionary<string, string> WorkRoutes, string workDirectory)
        {
            configResultPath = Path.Combine(workDirectory, "ResultConfiguration.json");
            _WorkRoutes = WorkRoutes;
        }

        public void ProcessConfigRequest(string configRequestPath)
        {
            string request = File.ReadAllText(configRequestPath);
            JObject objeto = JObject.Parse(request);
            var command = objeto.Property("command").Value;

            File.Delete(configRequestPath);

            switch (command.Value<string>())
            {
                case "getConfiguration":
                    WriteWorkRoutes();
                    break;
                case "setConfiguration":
                    setConfiguration(objeto);
                    break;
                default:
                    break;
            }


            Console.WriteLine(command);


            //string serialized = Newtonsoft.Json.JsonConvert.SerializeObject(objeto);


        }

        private void setConfiguration(JObject configuration)
        {
            string[] result = new string[] { "Configuración modificada" };

            try
            {
                var rutadiccionario = configuration.Property("rutadiccionario").Value.Value<string>();
                var rutamodulo = configuration.Property("rutamodulo").Value.Value<string>();
                var rutaservicio = configuration.Property("rutaservicio").Value.Value<string>();
                var rutamodelo = configuration.Property("rutamodelo").Value.Value<string>();
                var rutaroutingapp = configuration.Property("rutaroutingapp").Value.Value<string>();

                _WorkRoutes["dictionaryPath"] = rutadiccionario;
                _WorkRoutes["modulePath"] = rutamodulo;
                _WorkRoutes["servicePath"] = rutaservicio;
                _WorkRoutes["modelPath"] = rutamodelo;
                _WorkRoutes["routingPath"] = rutaroutingapp;
            }
            catch (Exception e)
            {
                result = new string[] { "Error", "Modificando la configuración", $"{e.Message}" };
            }

            File.WriteAllLines(configResultPath, result);
        }

        private void WriteWorkRoutes()
        {
            string dictionaryPath = _WorkRoutes["dictionaryPath"].Replace(@"\", @"\\");
            string modulePath = _WorkRoutes["modulePath"].Replace(@"\", @"\\");
            string servicePath = _WorkRoutes["servicePath"].Replace(@"\", @"\\");
            string modelPath = _WorkRoutes["modelPath"].Replace(@"\", @"\\");
            string routingPath = _WorkRoutes["routingPath"].Replace(@"\", @"\\");

            File.WriteAllLines(configResultPath, new string[]
            {
                        "{",
                        "  \"command\":\"setConfiguration\",",
                        $"  \"rutadiccionario\":\"{dictionaryPath}\",",
                        $"  \"rutamodulo\":\"{modulePath}\",",
                        $"  \"rutaservicio\":\"{servicePath}\",",
                        $"  \"rutamodelo\":\"{modelPath}\",",
                        $"  \"rutaroutingapp\":\"{routingPath}\"",
                        "}"
            });
        }

    }
}

