﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace TemplateManager
{
    public enum TransformationTypeEnum
    {
        None,
        ToLower,
        FromField,
        FormattValues
    }

    public enum ObjectType
    {
        ListColumn,
        Dictionary,
        FormField
    }

    public enum ValueTypeEnum
    {
        SingleValue,
        Property,
        Field
    }

    public enum ActionTypeEnum
    {
        NewFile,
        ModifyFile
    }

    public class ModuleReferenceDefinition
    {
        public string importModule { get; set; }
        public string moduleName { get; set; }
        public string[] functions { get; set; }
        public string functionsText { get; set; }
        public string importComponent { get; set; }
        public string viewchilds { get; set; }
        public string onhidenpopup { get; set; }
        public string onsetdefaulvalue { get; set; }
    }


    public class KeyValuePair {
        public string Name { get; set; }
        public string Value { get; set; }
    }

    public class GridConfiguration
    {
        public int SpanLabel { get; set; }
        public int SpanField { get; set; }
        public string AutoCompleteBehavior { get; set; }
    }

    public class ObjectDefinition {
        public string Name { get; set; }
        public string Type { get; set; }
        public int Width { get; set; }
        public int Heigth { get; set; }
        public string English { get; set; }
        public string Spanish { get; set; }
        public string ObjectType { get; set; }
        public string CellTemplate { get; set; }
        public List<KeyValuePair> TemplateParameters { get; set; }
        public List<KeyValuePair> Preload { get; set; }
        public List<CellRendererOption> CellRenderer { get; set; }
        public string Group { get; set; }
        public Boolean DetailParameter { get; set; }
        public Boolean DetailName { get; set; }
        public string BehaviorInEdition { get; set; }
    }

    public class CellRendererOption {
        public string Value { get; set; }
        public string Icon { get; set; }
        public string Color { get; set; }
        public string Text { get; set; }
    }

    public class TokenDefinition {
        public ValueTypeEnum ValueType { get; set; }
        public TransformationTypeEnum TransformationType { get; set; }
        public string Pattern { get; set; }
        public string AditionalInfo { get; set; }
    }

    public class ActionDefinition
    {
        public ActionTypeEnum ActionType { get; set; }
        public string MyProperty { get; set; }
        public string FileTemplatePath { get; set; }
        public string FilePath { get; set; }
        public string TemplateName { get; set; }
        public Dictionary<string, TokenDefinition> Patterns { get; set; }
    }

    //public class ComponentDefinition {
    //    public Dictionary<string, string> componentProperties { get; set; }
    //    public FieldDefinition[] Fields { get; set; }
    //    public List<ActionDefinition> Files { get; set; }
    //}

    public class ModuleDefinition
    {
        public string App { get; set; }
        //public string ModuleName { get; set; }
        public ComponentDefinition[] ComponentList { get; set; }
    }

    public class ComponentDefinition
    {
        public string type { get; set; }
        public string ComponentName { get; set; }
        public bool Principal { get; set; }
        public string Texto { get; set; }
        public string Icono { get; set; }
        public string Get { get; set; }
        public string GetBy { get; set; }
        public string SearchFunction { get; set; }
        public string SummaryFunction { get; set; }
        public string Post { get; set; }
        public string Put { get; set; }
        public string Delete { get; set; }
        public Binding Binding { get; set; }
        public string Parent { get; set; }
        public string SelectionCode { get; set; }
        public string SummaryCode { get; set; }
        public string SummaryName { get; set; }
        public GridConfiguration GridConfiguration { get; set; }
        public ObjectDefinition[] ObjectsList { get; set; }
    }

    public class Binding{
        public string LocalField { get; set; }
        public string ForeignField { get; set; }
    }

    //public class GeneralObject
    //{
    //    public List<object> imports { get; set; }
    //    public List<object> providers { get; set; }
    //    public List<object> declarations { get; set; }
    //}

}
