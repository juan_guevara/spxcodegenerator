﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TemplateManager
{
    public class ModuleManager
    {
        //public void RegisterModule(ComponentDefinition job, ModuleDefinition module, List<ModuleDefinition> modules)
        public void RegisterModule(ComponentDefinition job, ModuleDefinition module)
        {
            string ComponentName = job.ComponentName;
            string templatesPath = System.Configuration.ConfigurationManager.AppSettings.Get("templatesPath");
            string routingappTemplatePath = Path.Combine(templatesPath, "typescript.json");
            string htmlTemplatePath = Path.Combine(templatesPath, "html.json");
            string routingTemplatePath = Path.Combine(templatesPath, "typescript.json");
            string moduleTemplatePath = Path.Combine(templatesPath, "typescript.json");
            string microappPath = System.Configuration.ConfigurationManager.AppSettings.Get("microappPath");
            string routingPath = System.Configuration.ConfigurationManager.AppSettings.Get("routingPath");

            FilesBuilder fb = new FilesBuilder();

            List<string> paths = new List<string>(){
                    microappPath,
                };

            foreach (var path in paths)
            {
                CreateDirectory(path);
            }

            Dictionary<string, string> patterns = new Dictionary<string, string>();

            string moduleDirectory = Path.Combine(microappPath, "microapps", module.App);
            CreateDirectory(moduleDirectory);

            microappPath = Path.Combine(moduleDirectory, string.Format(@"{0}.module.ts", module.App.ToLower()));

            //Modulo
            CreateModule(job, htmlTemplatePath, module, ComponentName, moduleTemplatePath, microappPath);

            string moduleRoutingPath = Path.Combine(moduleDirectory, string.Format(@"{0}.routing.module.ts", module.App.ToLower()));

            string routingFileContent = string.Empty;

            if (!File.Exists(moduleRoutingPath))
            {
                //Routing
                patterns = new Dictionary<string, string>(){
                    {@"\${1:([^\}]*)\}", GetModuleRoutesReferences(routingTemplatePath, module, routingFileContent)},
                    {@"\${2:([^\}]*)\}", module.App.ToLower()},
                    {@"\${3:([^\}]*)\}", GetModuleRoutes(routingTemplatePath, module)}
                };

                routingFileContent = fb.BuildContent(routingTemplatePath, "routing", patterns);
            }
            else {
                routingFileContent = ModifyRoutingModule(moduleRoutingPath, routingTemplatePath, module);
            }

            File.WriteAllText(moduleRoutingPath, routingFileContent);

            //Thread.Sleep(2000);

            //routingapp
            RegisterModuleInAppRouting(routingPath, routingappTemplatePath, module);
        }

        private string ModifyRoutingModule( string moduleRoutingPath, string routingTemplatePath, ModuleDefinition module) {
            
            string result = string.Empty;
            result = File.ReadAllText(moduleRoutingPath);
            string componentsImports = GetModuleRoutesReferences(routingTemplatePath, module, result);
            string initialStringFinded = @"routes: Routes = [";

            if (!result.Contains(componentsImports)) {
                string componentsText = GetModuleRoutes(routingTemplatePath, module);
                result = $"{componentsImports}\r\n{result}";
                string originalContent = result;
                result = result.Substring(result.IndexOf(initialStringFinded) + initialStringFinded.Length, result.Length - result.IndexOf(initialStringFinded) - initialStringFinded.Length);
                string fragment = result;
                result = string.Format("\r\n{0}{1}", componentsText, result);
                result = originalContent.Replace(fragment, result);
            }

            return result;
        }

        private static void CreateDirectory(string path)
        {
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
        }

        private void RegisterModuleInAppRouting(string routingPath, string routingappTemplatePath, ModuleDefinition module)
        {
            FilesBuilder fb = new FilesBuilder();

            Dictionary<string, string> patterns = new Dictionary<string, string>();

            patterns = new Dictionary<string, string>(){
                    {@"\${1:([^\}]*)\}", module.App.ToLower()},
                    {@"\${2:([^\}]*)\}", module.App},
                    {@"\${3:([^\}]*)\}", module.App.ToLower()}
                };

            string routingText = fb.BuildContent(routingappTemplatePath, "routingapp", patterns);

            string initialStringFinded = @"component: FullLayoutComponent";
            String content = File.ReadAllText(routingPath);
            //string modulename = module.ModuleName.ToString();

            if (!content.Contains($"'{module.App.ToLower()}'"))
            {
                string originalContent = content;
                content = content.Substring(content.IndexOf(initialStringFinded) + initialStringFinded.Length, content.Length - content.IndexOf(initialStringFinded) - initialStringFinded.Length);

                initialStringFinded = @"children:";
                content = content.Substring(content.IndexOf(initialStringFinded) + initialStringFinded.Length, content.Length - content.IndexOf(initialStringFinded) - initialStringFinded.Length);
                content = content.Substring(2, content.Length - 2);
                string fragment = content;

                content = string.Format("{0}\r\n{1}", routingText, content);

                content = originalContent.Replace(fragment, content);

                File.WriteAllText(routingPath, content);
            }
            else
            {
                //throw new Exception(string.Format("El modulo {0} ya se encuentra registrado en el routing.", modulename));
                //Informacion el modulo ya se encuentra registrado en el routing.
            }
        }

        private void CreateModule(ComponentDefinition job, string htmlTemplatePath, ModuleDefinition module, string ComponentName, string moduleTemplatePath, string modulePath)
        {
            FilesBuilder fb = new FilesBuilder();

            Dictionary<string, string> patterns = new Dictionary<string, string>();

            ModuleReferenceDefinition fieldTemplateReferences = CreateComponentReferences(job.ObjectsList, htmlTemplatePath, job.ComponentName);

            if (!File.Exists(modulePath))
            {
                //patterns = new Dictionary<string, string>(){
                //     {@"\${1:([^\}]*)\}", string.Format("{0}RoutingModule",module.App.ToLower())}
                //    ,{@"\${2:([^\}]*)\}", ComponentName}
                //    ,{@"\${3:([^\}]*)\}", GetComponentNames(module)}
                //    ,{@"\${4:([^\}]*)\}", string.Format("{0}Module",module.App.ToLower())}
                //    ,{@"\${5:([^\}]*)\}", ComponentName.ToLower()}
                //    ,{@"\${6:([^\}]*)\}", fieldTemplateReferences.importModule}
                //    ,{@"\${7:([^\}]*)\}", fieldTemplateReferences.moduleName}
                //    ,{@"\${8:([^\}]*)\}", module.App.ToLower()}
                //    ,{@"\${9:([^\}]*)\}", CreateImportComponents(module, moduleTemplatePath)}
                //    ,{@"\${10:([^\}]*)\}", $"import {{ SR{ComponentName.ToLower()}Component }} from \"./pages/{module.ModuleName}/{ComponentName.ToLower()}/SR{ComponentName.ToLower()}/SR{ComponentName.ToLower()}.component\" "}
                //    ,{@"\${11:([^\}]*)\}", $"SR{ComponentName.ToLower()}Component" }
                //    ,{@"\${12:([^\}]*)\}", $"import {{ DT{ComponentName.ToLower()}Component }} from \"./pages/{module.ModuleName}/{ComponentName.ToLower()}/DT{ComponentName.ToLower()}/DT{ComponentName.ToLower()}.component\" "}
                //    ,{@"\${13:([^\}]*)\}", $"DT{ComponentName.ToLower()}Component" }
                //    ,{@"\${14:([^\}]*)\}", $"{CultureInfo.InvariantCulture.TextInfo.ToTitleCase(module.App)}" }
                //};

                patterns = new Dictionary<string, string>(){
                     {@"\${1:([^\}]*)\}", string.Format("{0}RoutingModule",module.App.ToLower())}
                    ,{@"\${2:([^\}]*)\}", ComponentName}
                    ,{@"\${3:([^\}]*)\}", GetComponentNames(module)}
                    ,{@"\${4:([^\}]*)\}", string.Format("{0}Module",module.App.ToLower())}
                    ,{@"\${5:([^\}]*)\}", ComponentName.ToLower()}
                    ,{@"\${6:([^\}]*)\}", fieldTemplateReferences.importModule}
                    ,{@"\${7:([^\}]*)\}", fieldTemplateReferences.moduleName}
                    ,{@"\${8:([^\}]*)\}", module.App.ToLower()}
                    ,{@"\${9:([^\}]*)\}", CreateImportComponents(module, moduleTemplatePath)}
                    ,{@"\${10:([^\}]*)\}", $"import {{ SR{ComponentName.ToLower()}Component }} from \"./pages/{ComponentName.ToLower()}/SR{ComponentName.ToLower()}/SR{ComponentName.ToLower()}.component\" "}
                    ,{@"\${11:([^\}]*)\}", $"SR{ComponentName.ToLower()}Component" }
                    ,{@"\${12:([^\}]*)\}", $"import {{ DT{ComponentName.ToLower()}Component }} from \"./pages/{ComponentName.ToLower()}/DT{ComponentName.ToLower()}/DT{ComponentName.ToLower()}.component\" "}
                    ,{@"\${13:([^\}]*)\}", $"DT{ComponentName.ToLower()}Component" }
                    ,{@"\${14:([^\}]*)\}", $"{CultureInfo.InvariantCulture.TextInfo.ToTitleCase(module.App)}" }
                };

                File.WriteAllText(modulePath, fb.BuildContent(moduleTemplatePath, "modulo", patterns));
                ModifyModule(modulePath, module);
                //Thread.Sleep(1000);
            }
            else {
                ModifyModule(modulePath, module);
            }
        }

        private void ModifyModule(string moduleTemplatePath, ModuleDefinition module)
        {
            string principalComponentName = module.ComponentList.First<ComponentDefinition>(x => x.Principal).ComponentName;
            List<string> newLines = new List<string>();
            newLines.Add($"import {{ Lst{principalComponentName}Component }} from './pages/{principalComponentName}/lst{principalComponentName}/lst{principalComponentName}.component';");
            newLines.Add($"import {{ SR{principalComponentName}Component }} from './pages/{principalComponentName}/SR{principalComponentName}/SR{principalComponentName}.component';");
            newLines.Add($"import {{ DT{principalComponentName}Component }} from './pages/{principalComponentName}/DT{principalComponentName}/DT{principalComponentName}.component';");
            string content = File.ReadAllText(moduleTemplatePath);
            foreach (var component in module.ComponentList)
            {
                if (!component.Principal)
                {
                    //string validation = "import { { { component.ComponentName.ToLower()} Component } } from './pages/{principalComponentName}/{component.ComponentName.ToLower()}/{component.ComponentName.ToLower()}.component'; ";
                    //if (!content.Contains(validation))
                    //{
                        newLines.Add($"import {{ {component.ComponentName.ToLower()}Component }} from './pages/{principalComponentName}/{component.ComponentName.ToLower()}/{component.ComponentName.ToLower()}.component';");
                    //}
                }
            }

            foreach (var newLine in newLines) {
                content = content.Replace(newLine, string.Empty);
                content = $"{newLine}\r\n{content}";
            }

            newLines = new List<string>();
            newLines.Add($"\t\t\t\tSR{principalComponentName}Component,");
            newLines.Add($"\t\t\t\tDT{principalComponentName}Component,");
            newLines.Add($"\t\t\t\t{GetComponentNames(module)}");

            string componentsText = string.Empty;
            foreach (var newLine in newLines)
            {
                content = content.Replace(newLine, string.Empty);
                componentsText = $"{newLine}{componentsText}";
            }

            FilesBuilder fb = new FilesBuilder();
            string initialStringFinded = @"declarations: [";

            string originalContent = content;
            content = content.Substring(content.IndexOf(initialStringFinded) + initialStringFinded.Length, content.Length - content.IndexOf(initialStringFinded) - initialStringFinded.Length);
            string fragment = content;
            content = string.Format("\r\n{0}{1}", componentsText, content);
            content = originalContent.Replace(fragment, content);
            File.WriteAllText(moduleTemplatePath, content);

            //string principalComponentName = module.ComponentList.First<ComponentDefinition>(x => x.Principal).ComponentName;
            //string importcomponents = $"import {{ Lst{principalComponentName}Component }} from './pages/{module.ModuleName}/{principalComponentName}/lst{principalComponentName}/lst{principalComponentName}.component';";
            //importcomponents = $"{importcomponents}\r\nimport {{ SR{principalComponentName}Component }} from './pages/{module.ModuleName}/{principalComponentName}/SR{principalComponentName}/SR{principalComponentName}.component';";
            //importcomponents = $"{importcomponents}\r\nimport {{ DT{principalComponentName}Component }} from './pages/{module.ModuleName}/{principalComponentName}/DT{principalComponentName}/DT{principalComponentName}.component';";


            //string principalComponentName = module.ComponentList.First<ComponentDefinition>(x => x.Principal).ComponentName;
            //string importcomponents = $"import {{ Lst{principalComponentName}Component }} from './pages/{principalComponentName}/lst{principalComponentName}/lst{principalComponentName}.component';";
            //importcomponents = $"import {{ SR{principalComponentName}Component }} from './pages/{principalComponentName}/SR{principalComponentName}/SR{principalComponentName}.component';\r\n{importcomponents}";
            //importcomponents = $"import {{ DT{principalComponentName}Component }} from './pages/{principalComponentName}/DT{principalComponentName}/DT{principalComponentName}.component';\r\n{importcomponents}";

            //foreach (var component in module.ComponentList)
            //{
            //    if (!component.Principal)
            //    {
            //        importcomponents = $"{importcomponents}\r\nimport {{ {component.ComponentName.ToLower()}Component }} from './pages/{module.ModuleName}/{principalComponentName}/{component.ComponentName.ToLower()}/{component.ComponentName.ToLower()}.component';";
            //    }
            //}            

            //string componentsText = $"\t\t\t\tSR{principalComponentName}Component,\r\n";
            //componentsText = $"{componentsText}\t\t\t\tDT{principalComponentName}Component,\r\n";
            //componentsText = $"{componentsText}\t\t\t\t{GetComponentNames(module)}";



            //if (!content.Contains(importcomponents)) {
            //    content = $"{importcomponents}\r\n{content}";
            //}

            //if (!content.Contains(componentsText))
            //{
            //string originalContent = content;
            //content = content.Substring(content.IndexOf(initialStringFinded) + initialStringFinded.Length, content.Length - content.IndexOf(initialStringFinded) - initialStringFinded.Length);
            //string fragment = content;
            //content = string.Format("\r\n{0}{1}", componentsText, content);
            //content = originalContent.Replace(fragment, content);
            //File.WriteAllText(moduleTemplatePath, content);
            //}
            //else
            //{
            //    //throw new Exception(string.Format("El modulo {0} ya se encuentra registrado en el routing.", modulename));
            //    //Informacion el modulo ya se encuentra registrado en el routing.
            //}
        }

        private string GetModuleRoutesReferences(string routingTemplatePath, ModuleDefinition module, string fileContent)
        {
            FilesBuilder fb = new FilesBuilder();
            string routeReferences = string.Empty;
            List<string> components = new List<string>();

            foreach (var component in module.ComponentList)
            {
                if (component.Principal)
                {
                    components.Add(component.ComponentName);

                    //Dictionary<string, string> patterns = new Dictionary<string, string>(){
                    //        {@"\${1:([^\}]*)\}", component.ComponentName},
                    //        {@"\${2:([^\}]*)\}", component.ComponentName.ToLower()},
                    //        {@"\${3:([^\}]*)\}", module.ModuleName.ToLower()}
                    //    };

                    Dictionary<string, string> patterns = new Dictionary<string, string>(){
                            {@"\${1:([^\}]*)\}", component.ComponentName},
                            {@"\${2:([^\}]*)\}", component.ComponentName.ToLower()}
                        };

                    string newRouteReference = fb.BuildContent(routingTemplatePath, "routingModuleReferences", patterns);

                    if (!fileContent.Contains(newRouteReference))
                    {
                        routeReferences = string.Format("{0}\n{1}", routeReferences, newRouteReference);
                    }
                }
            }

            return routeReferences;
        }

        private string GetModuleRoutes(string routingTemplatePath, ModuleDefinition module)
        {
            FilesBuilder fb = new FilesBuilder();

            string routes = string.Empty;
            ComponentDefinition[] components = module.ComponentList;
            foreach (ComponentDefinition component in components)
            {
                if (component.Principal)
                {
                    string path = string.Empty;
                    //if (!component.Principal)
                    //{
                    path = component.ComponentName.ToLower();
                    //}

                    string newRoute = string.Empty;
                    Dictionary<string, string> patterns = new Dictionary<string, string>(){
                        {@"\${1:([^\}]*)\}", component.ComponentName},
                        {@"\${2:([^\}]*)\}", path}
                    };

                    newRoute = fb.BuildContent(routingTemplatePath, "routingModuleRoutes", patterns);
                    routes = string.Format("{0}\n{1}", routes, newRoute);
                }
            }

            return routes;
        }

        private ModuleReferenceDefinition CreateComponentReferences(ObjectDefinition[] fields, string templatePath, string jobName)
        {
            FilesBuilder fb = new FilesBuilder();
            List<ModuleReferenceDefinition> componentReferences = new List<ModuleReferenceDefinition>();
            ModuleReferenceDefinition result = new ModuleReferenceDefinition() { importModule = string.Empty, moduleName = string.Empty };
            result.functionsText = string.Empty;
            result.importComponent = string.Empty;
            result.viewchilds = string.Empty;
            result.onhidenpopup = string.Empty;
            result.onsetdefaulvalue = string.Empty;

            string currentTemplate = "";

            foreach (var item in fields)
            {
                Dictionary<string, string> patterns = new Dictionary<string, string>();
                patterns.Add(@"\${1:([^\}]*)\}", item.Name);

                if (item.ObjectType.Equals("FormField") && !string.IsNullOrEmpty(item.CellTemplate) && !item.CellTemplate.Equals(currentTemplate, StringComparison.InvariantCultureIgnoreCase))
                {
                    ModuleReferenceDefinition definition = fb.GetComponentReferences(templatePath, item.CellTemplate);
                    if (!string.IsNullOrEmpty(definition.importModule)
                        && !result.importModule.Contains(definition.importModule)
                        && !item.CellTemplate.Contains("Generic"))
                    {
                        result.importModule = string.Format("{0}{1}{2}", result.importModule, definition.importModule, Environment.NewLine);
                    }

                    if (!string.IsNullOrEmpty(definition.moduleName)
                        && !result.moduleName.Contains(definition.moduleName))
                    {
                        result.moduleName = string.Format("{0}{1},{2}", result.moduleName, definition.moduleName, Environment.NewLine);
                    }

                    if (!string.IsNullOrEmpty(definition.importComponent)
                        && !result.importComponent.Contains(definition.importComponent))
                    {
                        result.importComponent = string.Format("{0}{1}{2}", result.importComponent, fb.Replace(definition.importComponent, patterns), Environment.NewLine);
                    }

                    if (!string.IsNullOrEmpty(definition.viewchilds)
                        && !result.viewchilds.Contains(definition.viewchilds))
                    {
                        result.viewchilds = string.Format("{0}{1}{2}", result.viewchilds, fb.Replace(definition.viewchilds, patterns), Environment.NewLine);
                    }

                    if (!string.IsNullOrEmpty(definition.onhidenpopup)
                        && !result.onhidenpopup.Contains(definition.onhidenpopup))
                    {
                        result.onhidenpopup = string.Format("{0}{1}{2}", result.onhidenpopup, fb.Replace(definition.onhidenpopup, patterns), Environment.NewLine);
                    }

                    if (!string.IsNullOrEmpty(definition.onsetdefaulvalue)
                        && !result.onsetdefaulvalue.Contains(definition.onsetdefaulvalue))
                    {
                        result.onsetdefaulvalue = string.Format("{0}{1}{2}", result.onsetdefaulvalue, fb.Replace(definition.onsetdefaulvalue, patterns), Environment.NewLine);
                    }

                    foreach (var functionLine in definition.functions)
                    {
                        result.functionsText = string.Format("{0}{1}{2}", result.functionsText, fb.Replace(functionLine, patterns), Environment.NewLine);
                    }
                }
            }

            return result;
        }

        private string GetComponentNames(ModuleDefinition module)
        {
            string result = string.Empty;
            foreach (var component in module.ComponentList)
            {
                if (component.Principal)
                {
                    result = $"        Lst{component.ComponentName}Component,\n{result}";
                    //result = string.Format("{0},{1}", string.Format("Lst{0}Component\n", component.ComponentName), result);
                }
                else
                {
                    result = $"{component.ComponentName}Component,\n{result}";
                    //result = string.Format("{0},{1}", string.Format("{0}Component\n", component.ComponentName), result);
                }
            }

            return result.TrimEnd(new char[] { ',' });
        }

        private string CreateImportComponents(ModuleDefinition module, string templatePath)
        {
            string result = string.Empty;

            FilesBuilder fb = new FilesBuilder();
            string principalComponentName = module.ComponentList.First<ComponentDefinition>(x => x.Principal).ComponentName;
            foreach (var component in module.ComponentList)
            {
                //Dictionary<string, string> patterns = new Dictionary<string, string>(){
                //    {@"\${1:([^\}]*)\}", component.ComponentName},
                //    {@"\${2:([^\}]*)\}", component.ComponentName.ToLower()},
                //    {@"\${3:([^\}]*)\}", module.ModuleName.ToLower()},
                //    {@"\${4:([^\}]*)\}", principalComponentName.ToLower()}
                //    };

                Dictionary<string, string> patterns = new Dictionary<string, string>(){
                    {@"\${1:([^\}]*)\}", component.ComponentName},
                    {@"\${2:([^\}]*)\}", component.ComponentName.ToLower()},
                    {@"\${4:([^\}]*)\}", principalComponentName.ToLower()}
                    };

                if (component.Principal)
                {
                    result = string.Format("{0}{1}", fb.BuildContent(templatePath, "importPrincipalModuleComponent", patterns), result);
                }
                else
                {
                    result = string.Format("{0}{1}", fb.BuildContent(templatePath, "importModuleComponent", patterns), result);
                }
            }

            return result.TrimStart(new char[] { '\r' }).TrimStart(new char[] { '\n' });
        }

    }
}
