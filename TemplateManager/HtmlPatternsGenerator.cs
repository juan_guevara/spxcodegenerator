﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace TemplateManager
{
    public static class HtmlPatternsGenerator
    {
        public static Dictionary<string, string> BuildPatterns(ComponentDefinition component, string htmlTemplatePath, ModuleDefinition module) {
            switch (component.type)
            {
                case "listDevexpressGrid":
                    return BuildListDevexpressGrid(component, htmlTemplatePath, module);

                case "listAgGrid":
                    return BuildListAgGrid(component, htmlTemplatePath, module);

                default:
                    return BuildListDevexpressGrid(component, htmlTemplatePath, module);
            }
        }

        public static Dictionary<string, string> BuildListDevexpressGrid(ComponentDefinition component, string htmlTemplatePath, ModuleDefinition module)
        {
            Dictionary<string, string> patterns = new Dictionary<string, string>(){
                     {@"\${1:([^\}]*)\}", component.ComponentName}
                    ,{@"\${2:([^\}]*)\}", CreateDevexpressColumnLines(component.ObjectsList, component.ComponentName, "ListColumn")}
                    ,{@"\${3:([^\}]*)\}", GetFormColumnsCount(component.ObjectsList, 1).ToString()}
                    ,{@"\${4:([^\}]*)\}", CreateGroup(component.ObjectsList, htmlTemplatePath, component.ComponentName)}
                    ,{@"\${5:([^\}]*)\}", component.ComponentName.ToLower()}
                    ,{@"\${6:([^\}]*)\}", CreateChildComponentVisualization(module, htmlTemplatePath, component.ComponentName)}
                    ,{@"\${7:([^\}]*)\}", CreateContextMenu(htmlTemplatePath, component.ComponentName, module)}
                };
            return patterns;
        }

        public static Dictionary<string, string> BuildListAgGrid(ComponentDefinition component, string htmlTemplatePath, ModuleDefinition module)
        {
            Dictionary<string, string> patterns = new Dictionary<string, string>(){
                     {@"\${1:([^\}]*)\}", AddAgGrid(htmlTemplatePath, module)}
                    ,{@"\${3:([^\}]*)\}", GetFormColumnsCount(component.ObjectsList, 1).ToString()}
                    ,{@"\${4:([^\}]*)\}", CreateGroup(component.ObjectsList, htmlTemplatePath, component.ComponentName)}
                    ,{@"\${5:([^\}]*)\}", component.ComponentName.ToLower()}
                    ,{@"\${6:([^\}]*)\}", CreateChildComponentVisualization(module, htmlTemplatePath, component.ComponentName)}
                    ,{@"\${7:([^\}]*)\}", CreateContextMenu(htmlTemplatePath, component.ComponentName, module)}
                };
            return patterns;
        }


        private static string CreateDevexpressColumnLines(ObjectDefinition[] modelProperties, string componentName, string objectType)
        {
            try
            {
                string columnsLines = string.Empty;

                foreach (var item in modelProperties)
                {
                    string cellTemplate = string.Empty;
                    string width = string.Empty;
                    string dateFormatt = string.Empty;

                    if (item.ObjectType.Equals(objectType))
                    {
                        if (!string.IsNullOrEmpty(item.CellTemplate))
                        {
                            switch (item.CellTemplate.ToLower())
                            {
                                case "celltemplatecode":
                                    cellTemplate = "cellTemplate=\"cellTemplateCode\" ";
                                    break;
                                default:
                                    cellTemplate = string.Empty;
                                    break;
                            }
                        }

                        if (item.Width > 0)
                        {
                            width = string.Format(" width=\"{0}\" ", item.Width);
                        }

                        if (item.Type.ToLower().Contains("date"))
                        {
                            dateFormatt = " dataType=\"datetime\" format=\"dd/MM/yyyy HH:mm\" ";
                        }

                        columnsLines = string.Format("{0} {1}     <dxi-column [showInColumnChooser]=\"true\" {3}dataField=\"{4}\" {8}{9}caption=\"{6}{6}'lst{5}.{2}' | translate {7}{7}\"></dxi-column>", columnsLines, Environment.NewLine, item.Name.ToUpper(), width, item.Name, componentName.ToLower(), (char)123, (char)125, cellTemplate, dateFormatt);
                    }
                }

                return columnsLines;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private static int GetFormColumnsCount(ObjectDefinition[] fields, int maxColumnsByForm)
        {
            string FormField = string.Empty;

            int groupsCount = 0;
            string currentColumn = "";
            foreach (var item in fields)
            {
                if (item.ObjectType.Equals("FormField") && !string.IsNullOrEmpty(item.Group) && !item.Group.Equals(currentColumn, StringComparison.InvariantCultureIgnoreCase))
                {
                    currentColumn = item.Group;
                    groupsCount = groupsCount + 1;
                }
            }

            if (groupsCount > maxColumnsByForm)
                groupsCount = maxColumnsByForm;

            return groupsCount;
        }

        public static string CreateGroup(ObjectDefinition[] fields, string templatePath, string componentName)
        {
            FilesBuilder fb = new FilesBuilder();
            string result = string.Empty;

            string currentColumn = "";

            foreach (var item in fields)
            {
                if (item.ObjectType.Equals("FormField") && !string.IsNullOrEmpty(item.Group) && !item.Group.Equals(currentColumn, StringComparison.InvariantCultureIgnoreCase))
                {
                    Dictionary<string, string> patterns = new Dictionary<string, string>(){
                    {@"\${1:([^\}]*)\}", item.Group},
                    {@"\${2:([^\}]*)\}", CreateGroupItems(fields, templatePath, componentName, item.Group)}};
                    result = string.Format("{0}{1}{2}", result, Environment.NewLine, fb.BuildContent(templatePath, "group", patterns));
                    currentColumn = item.Group;
                }
            }

            return result;
        }

        public static string CreateChildComponentVisualization(ModuleDefinition module, string templatePath, string componentName)
        {
            string containerType = "Tab";
            string result = string.Empty;

            FilesBuilder fb = new FilesBuilder();

            foreach (var component in module.ComponentList)
            {
                if (!string.IsNullOrEmpty(component.Parent) && component.Parent.Equals(componentName, StringComparison.InvariantCultureIgnoreCase))
                {
                    Dictionary<string, string> patterns = new Dictionary<string, string>(){
                    {@"\${1:([^\}]*)\}", component.ComponentName.ToLower()},
                    {@"\${2:([^\}]*)\}", component.ComponentName.ToLower()}
                    };
                    result = string.Format("{0}{1}{2}", Environment.NewLine, result, fb.BuildContent(templatePath, containerType, patterns));
                }
            }

            return result;
        }

        private static string CreateContextMenu(string htmlTemplatePath, string componentName, ModuleDefinition module)
        {
            FilesBuilder fb = new FilesBuilder();
            string contextMenu = string.Empty;

            ComponentDefinition component = module.ComponentList.First<ComponentDefinition>(x => x.ComponentName.Equals(componentName, StringComparison.InvariantCultureIgnoreCase));

            int count = component.ObjectsList.ToList<ObjectDefinition>().FindAll(x => x.ObjectType.Equals("MenuItem", StringComparison.InvariantCultureIgnoreCase)).Count;

            if (count > 0)
            {
                string newContextMenu = fb.BuildContent(htmlTemplatePath, "contextMenu", new Dictionary<string, string>());
                contextMenu = string.Format("{0}\n{1}", contextMenu, newContextMenu);
            }

            return contextMenu;
        }

        public static string CreateGroupItems(ObjectDefinition[] fields, string templatePath, string componentName, string group)
        {
            string fieldsInGroup = string.Empty;

            foreach (var item in fields)
            {
                if (item.ObjectType.Equals("FormField") && !string.IsNullOrEmpty(item.Group) && item.Group.Equals(group, StringComparison.InvariantCultureIgnoreCase))
                {
                    string visible = string.Empty;
                    string disable = string.Empty;
                    string width = string.Empty;
                    string widthNumber = string.Empty;

                    if (!string.IsNullOrEmpty(item.BehaviorInEdition))
                    {
                        switch (item.BehaviorInEdition.ToLower())
                        {
                            case "disable":
                                disable = " disabled: disableInputs";
                                break;

                            case "hide":
                                visible = "[visible]=\"visibleInputs\"";
                                break;
                        }
                    }
                    else
                    {
                        disable = " disabled: false";
                    }

                    if (item.Width > 0)
                    {
                        width = string.Format(" width:'{0}', ", item.Width.ToString());
                        widthNumber = string.Format("[width]=\"${0}\"", item.Width.ToString());
                    }

                    FilesBuilder fb = new FilesBuilder();
                    Dictionary<string, string> patterns = new Dictionary<string, string>(){
                    {@"\${1:([^\}]*)\}", item.Name},
                    {@"\${2:([^\}]*)\}", string.Format("lst{0}.{1}", componentName.ToLower(), item.Name.ToUpper())},
                    {@"\${3:([^\}]*)\}", visible},
                    {@"\${4:([^\}]*)\}", disable},
                    {@"\${5:([^\}]*)\}", width},
                    {@"\${6:([^\}]*)\}", widthNumber}
                    };

                    string template = item.CellTemplate;
                    //string template = string.Empty;
                    if (string.IsNullOrEmpty(template))
                    {
                        string itemType = homologateTypes(item.Type);
                        switch (itemType.ToLower())
                        {
                            case "date":
                                template = "Html_dateboxedicion";
                                break;
                            case "number":
                                template = "Html_numberboxedicion";
                                break;
                            default:
                                template = "Html_textboxedicion";
                                break;
                        }
                    }

                    if (item.TemplateParameters != null)
                    {
                        foreach (var parameter in item.TemplateParameters)
                        {
                            string patternText = @"\${" + parameter.Name + @"([^\}]*)\}";
                            string Value = parameter.Value;

                            patterns.Add(patternText, Value);
                        }
                    }


                    string field = fb.BuildContent(templatePath, template, patterns);


                    fieldsInGroup = string.Format("{0}{1}{2}", fieldsInGroup, Environment.NewLine, field);
                }
            }

            return fieldsInGroup;
        }

        private static string homologateTypes(string itemType)
        {
            string homologation = string.Empty;

            if (itemType.ToLower().Contains("date"))
            {
                homologation = "Date";
            }
            else
            {
                if (itemType.ToLower().Contains("char") || itemType.ToLower().Contains("string"))
                {
                    homologation = "string";
                }
                else
                {
                    if (itemType.ToLower().Contains("boolean"))
                    {
                        homologation = "boolean";
                    }
                    else
                    {
                        homologation = "Number";
                    }
                }
            }

            return homologation;
        }

        public static string AddAgGrid(string htmlTemplatePath, ModuleDefinition module)
        {
            FilesBuilder fb = new FilesBuilder();

            Dictionary<string, string> patterns = new Dictionary<string, string>();

            patterns = new Dictionary<string, string>() { };

            string gridText = fb.BuildContent(htmlTemplatePath, "agGrid", patterns);

            return gridText;
        }
    }
}
