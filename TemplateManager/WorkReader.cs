﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace TemplateManager
{
    public static class WorkReader
    {
        public static List<ModuleDefinition> Read(string templatePath)
        {
            string result = string.Empty;
            String content = File.ReadAllText(templatePath);
            var objeto = JObject.Parse(content);

            List<ModuleDefinition> jobs = new List<ModuleDefinition>();

            string appName = string.Empty;
            foreach (var item in objeto)
            {
                if (item.Key == "app")
                {
                    appName = item.Value.ToString();
                }
                else {
                    ModuleDefinition[] modules = item.Value.ToObject<ModuleDefinition[]>();
                    foreach (var module in modules)
                    {
                        module.App = appName;
                    }

                    jobs = modules.ToList<ModuleDefinition>();
                }
            }

            return jobs;
        }
    }
}
