﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TemplateManager;
using System.IO;

namespace CodeGenerator
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("CodeGenerator - hagamos codigo");
                Console.WriteLine("x para cerrar, cualquier otra tecla para ejecutar");
                Run();
            }
            catch (Exception e)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(string.Format("CodeGenerator - encontre un error: {0}", e.Message));
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine(Environment.NewLine);
                Console.WriteLine("CodeGenerator - ya no puedo seguir");
            }        
        }

        static void Run()
        {
            try
            {
                Worker worker = new Worker();
                while (true)
                {
                    string option = Console.ReadLine();
                    {
                        if (option.Equals("x", StringComparison.InvariantCultureIgnoreCase))
                            return;
                    }
                    Console.WriteLine("CodeGenerator - iniciando trabajo");
                    //worker.ExecuteWork();
                    Console.WriteLine("CodeGenerator - termino satisfactoriamente");
                }
            }
            catch (Exception e)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(string.Format("CodeGenerator - encontre un error: {0}", e.ToString()));
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine(Environment.NewLine);
                Console.WriteLine("CodeGenerator - Intentalo de nuevo");
                Run();
            }
        }
    }
}
